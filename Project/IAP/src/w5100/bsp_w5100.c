
//#include <includes.h>
#include <string.h>
//#include "bsp.h"
#include "bsp_w5100.h"
#include "boot_main.h"

 __IO unsigned char  wcnt = 0;



#ifdef _NET1_W5100_USE_
w5100_comm_reg_t* var_w5100_comm_reg = (w5100_comm_reg_t*)(W5100_BASE + W5100_COMM_REG_BASE);
w5100_sock_reg_t* var_w5100_sock_reg[4] ={ 	
	(w5100_sock_reg_t*)(W5100_BASE + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*0),
	(w5100_sock_reg_t*)(W5100_BASE + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*1),
	(w5100_sock_reg_t*)(W5100_BASE + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*2),
	(w5100_sock_reg_t*)(W5100_BASE + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*3),		
	};
unsigned char* w5100_tx_mem = (unsigned char*)(W5100_BASE + W5100_TX_MEM_BASE);
unsigned char* w5100_rx_mem = (unsigned char*)(W5100_BASE + W5100_RX_MEM_BASE);

pUNMAP_TABLE_HEAD w5100net1_punmap;
#endif

#ifdef _NET2_W5100_USE_
w5100_comm_reg_t* var_w5100_comm_reg2 = (w5100_comm_reg_t*)(W5100_BASE2 + W5100_COMM_REG_BASE);
w5100_sock_reg_t* var_w5100_sock_reg2[4] = {
	(w5100_sock_reg_t*)(W5100_BASE2 + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*0),
	(w5100_sock_reg_t*)(W5100_BASE2 + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*1),
	(w5100_sock_reg_t*)(W5100_BASE2 + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*2),
	(w5100_sock_reg_t*)(W5100_BASE2 + W5100_SOCK_REG_BASE+W5100_SOCK_REG_SIZE*3),
	};
unsigned char* w5100_tx_mem2 = (unsigned char*)(W5100_BASE2 + W5100_TX_MEM_BASE);
unsigned char* w5100_rx_mem2 = (unsigned char*)(W5100_BASE2 + W5100_RX_MEM_BASE);

pUNMAP_TABLE_HEAD w5100net2_punmap;
#endif

#if 1
u32  BSP_CPU_ClkFreq (void)
{
    RCC_ClocksTypeDef  rcc_clocks;


    RCC_GetClocksFreq(&rcc_clocks);

    return ((u32)rcc_clocks.HCLK_Frequency);
}


void BSP_Delay(u32 ms)
{
	__IO u32 loop = BSP_CPU_ClkFreq()/1000/5*ms;
	
	while(loop--);
}


#else
void BSP_Delay(int ms)
{
	//72*1000*1000/1000/5*ms;
	__IO int loop = (( (72*1000*1000)/500)/5)*ms;
	
	while(loop--);
}
#endif

//---------------------------------共用函数--------------------------------------//

void BSP_W5100_MemCopy(unsigned char *dst, unsigned char *src, unsigned short size)
{
	unsigned short i;
	for(i=0;i<size;i++)
	{
		*dst = *src;
		dst ++;
		src ++;
	}
}

void BSP_W5100_MemSet (unsigned char *dst, unsigned char data, unsigned short size)
{
	unsigned short i;
	for(i=0;i<size;i++)
	{
		*dst = data;
		dst ++;
	}
}

unsigned short BSP_W5100_MemCmp (unsigned char *dst, unsigned char *src, unsigned short size)
{
	unsigned short i;
	for(i=0;i<size;i++)
	{
		if(*dst != *src)
			return (i+1);
		dst ++;
		src ++;
	}
	return 0;
}
//---------------------------------共用函数----------------------------------------//
#ifdef _NET1_W5100_USE_
unsigned short BSP_W5100_SockSend(unsigned char sock_id, unsigned char* data, unsigned short len)
{
	unsigned short l;
	unsigned short wptr;
	unsigned short size;
	unsigned short off;
	unsigned char* base;
	
	wptr = var_w5100_sock_reg[sock_id]->tx_write_pointer[0];
	wptr = wptr << 8;
	wptr |= var_w5100_sock_reg[sock_id]->tx_write_pointer[1];
	size = var_w5100_sock_reg[sock_id]->tx_free_size[0];
	size = size << 8;
	size |= var_w5100_sock_reg[sock_id]->tx_free_size[1];
	
	if(size == 0)
		{
		APP_TRACE("\r\nsend size=0\r\n");
		return 0;
		}
	
	off = wptr & (W5100_SOCK_TX_MEM_SIZE - 1);
	len = W5100_MIN(len, size);
	l = W5100_MIN(len, W5100_SOCK_TX_MEM_SIZE - off);
	
	base = w5100_tx_mem + W5100_SOCK_TX_MEM_SIZE*sock_id;
	BSP_W5100_MemCopy(base + off, data, l);
	BSP_W5100_MemCopy(base + 0, data + l, len - l);
	
	//APP_TRACE("BSP_W5100_SockSend %d, len %d\n", sock_id, len);
	//APP_TRACE("wptr %04X, base %08X\n", wptr, (int)base);
	
	wptr += len;
	var_w5100_sock_reg[sock_id]->tx_write_pointer[0] = wptr >> 8;
	var_w5100_sock_reg[sock_id]->tx_write_pointer[1] = wptr;
	var_w5100_sock_reg[sock_id]->command = W5100_COMMAND_SEND;
	
	return len;
}

void BSP_W5100_SockInitUdp(unsigned char sock_id, unsigned short port)
{
	var_w5100_sock_reg[sock_id]->mode = 0x20 | W5100_PROTOCOL_UDP;
	var_w5100_sock_reg[sock_id]->src_port[0] = port >> 8;
	var_w5100_sock_reg[sock_id]->src_port[1] = port;
	var_w5100_sock_reg[sock_id]->command = W5100_COMMAND_OPEN;
	//APP_TRACE("BSP_W5100_SockInitUdp(%d, %d)\n", sock_id, port);
}

void BSP_W5100_Config1(void)
{	
//只启用sock 0 ,8K缓冲区

	var_w5100_comm_reg->mode = 0x80;//复位所有寄存器 rst 位会自动清0

		
		var_w5100_comm_reg->rx_mem_size = 0x03;//8k
		var_w5100_comm_reg->tx_mem_size = 0x03;//8k

#if 1
		var_w5100_comm_reg->gateway[0]= g_total_boot_desc_info->udp_local_info1.localhost_gateway[0];
		var_w5100_comm_reg->gateway[1]= g_total_boot_desc_info->udp_local_info1.localhost_gateway[1];
		var_w5100_comm_reg->gateway[2]= g_total_boot_desc_info->udp_local_info1.localhost_gateway[2];
		var_w5100_comm_reg->gateway[3]= g_total_boot_desc_info->udp_local_info1.localhost_gateway[3];
		
		var_w5100_comm_reg->subnet[0] =	g_total_boot_desc_info->udp_local_info1.localhost_subnet[0];
		var_w5100_comm_reg->subnet[1] =	g_total_boot_desc_info->udp_local_info1.localhost_subnet[1];
		var_w5100_comm_reg->subnet[2] =	g_total_boot_desc_info->udp_local_info1.localhost_subnet[2];
		var_w5100_comm_reg->subnet[3] =	g_total_boot_desc_info->udp_local_info1.localhost_subnet[3];
		
		var_w5100_comm_reg->src_ip[0] =	g_total_boot_desc_info->udp_local_info1.localhost_IP[0];
		var_w5100_comm_reg->src_ip[1] =	g_total_boot_desc_info->udp_local_info1.localhost_IP[1];
		var_w5100_comm_reg->src_ip[2] =	g_total_boot_desc_info->udp_local_info1.localhost_IP[2];
		var_w5100_comm_reg->src_ip[3] =	g_total_boot_desc_info->udp_local_info1.localhost_IP[3];


		u2_printf("var_w5100_comm_reg->gateway[0] is %d  \r\n",var_w5100_comm_reg->gateway[0]);
		u2_printf("var_w5100_comm_reg->gateway[1] is %d  \r\n",var_w5100_comm_reg->gateway[1]);
		u2_printf("var_w5100_comm_reg->gateway[2] is %d  \r\n",var_w5100_comm_reg->gateway[2]);
		u2_printf("var_w5100_comm_reg->gateway[3] is %d  \r\n",var_w5100_comm_reg->gateway[3]);

		u2_printf("var_w5100_comm_reg->subnet[0] is %d  \r\n",var_w5100_comm_reg->subnet[0]);
		u2_printf("var_w5100_comm_reg->subnet[1] is %d  \r\n",var_w5100_comm_reg->subnet[1]);
		u2_printf("var_w5100_comm_reg->subnet[2] is %d  \r\n",var_w5100_comm_reg->subnet[2]);
		u2_printf("var_w5100_comm_reg->subnet[3] is %d  \r\n",var_w5100_comm_reg->subnet[3]);

		
		u2_printf("var_w5100_comm_reg->src_ip[0] is %d  \r\n",var_w5100_comm_reg->src_ip[0]);
		u2_printf("var_w5100_comm_reg->src_ip[1] is %d  \r\n",var_w5100_comm_reg->src_ip[1]);
		u2_printf("var_w5100_comm_reg->src_ip[2] is %d  \r\n",var_w5100_comm_reg->src_ip[2]);
		u2_printf("var_w5100_comm_reg->src_ip[3] is %d  \r\n",var_w5100_comm_reg->src_ip[3]);
#else

		var_w5100_comm_reg->gateway[0]= 192;
		var_w5100_comm_reg->gateway[1]= 168;
		var_w5100_comm_reg->gateway[2]= 2;
		var_w5100_comm_reg->gateway[3]= 1;
		var_w5100_comm_reg->subnet[0] =	255;
		var_w5100_comm_reg->subnet[1] =	255;
		var_w5100_comm_reg->subnet[2] =	255;
		var_w5100_comm_reg->subnet[3] =	0;
		var_w5100_comm_reg->src_ip[0] =	192;
		var_w5100_comm_reg->src_ip[1] =	168;
		var_w5100_comm_reg->src_ip[2] =	2;
		var_w5100_comm_reg->src_ip[3] =	28;
#endif

		
              
#if 1
        var_w5100_comm_reg->src_mac[0]=	0xF4;
		var_w5100_comm_reg->src_mac[1]=	0xAA;
		var_w5100_comm_reg->src_mac[2]=	0x18;
		var_w5100_comm_reg->src_mac[3]=	0xE7;
		var_w5100_comm_reg->src_mac[4]=	0xF1;
		var_w5100_comm_reg->src_mac[5]=	0xE6;	
                                       
#else
		var_w5100_comm_reg->src_mac[0]=	0xc8;
		var_w5100_comm_reg->src_mac[1]=	0xc7;
		var_w5100_comm_reg->src_mac[2]=	0xc6;
		var_w5100_comm_reg->src_mac[3]=	0xc5;
		var_w5100_comm_reg->src_mac[4]=	0xc4;
		var_w5100_comm_reg->src_mac[5]=	0xc3;	
#endif
		
		var_w5100_comm_reg->int_mask =	//W5100_INT_MASK_CONFLICT |
									//W5100_INT_MASK_UNREACH	|
									//W5100_INT_MASK_PPPOE	|
									//W5100_INT_MASK_S3_INT	|
									//W5100_INT_MASK_S2_INT	|
									//W5100_INT_MASK_S1_INT	|
									W5100_INT_MASK_S0_INT	;

	#if 1
	u2_printf("localhost_UDPport is %d  \r\n",EXCHANGE16BIT(g_total_boot_desc_info->udp_local_info1.localhost_UDPport));
	BSP_W5100_SockInitUdp(0,EXCHANGE16BIT(g_total_boot_desc_info->udp_local_info1.localhost_UDPport));	
	#else
	BSP_W5100_SockInitUdp(0,4000);
	#endif
}

unsigned short BSP_W5100_SockRecv(unsigned char sock_id, unsigned char* data, unsigned short len)
{
	unsigned short l;
	unsigned short rptr;
	unsigned short size;
	unsigned short off;
	unsigned char* base;
	
	rptr = var_w5100_sock_reg[sock_id]->rx_read_pointer[0];
	rptr = rptr << 8;
	rptr |= var_w5100_sock_reg[sock_id]->rx_read_pointer[1];
	size = var_w5100_sock_reg[sock_id]->rx_received_size[0];
	size = size << 8;
	size |= var_w5100_sock_reg[sock_id]->rx_received_size[1];
	
	if(size == 0)
		{
		APP_TRACE("\r\nrecv size=0\r\n");
		return 0;
		}
	off = rptr & (W5100_SOCK_RX_MEM_SIZE - 1);
	len = W5100_MIN(len, size);
	l = W5100_MIN(len, W5100_SOCK_RX_MEM_SIZE - off);
	
	base = w5100_rx_mem + W5100_SOCK_RX_MEM_SIZE*sock_id;
	BSP_W5100_MemCopy(data, base + off, l);
	BSP_W5100_MemCopy(data + l, base + 0, len - l);

	//APP_TRACE("BSP_W5100_SockRecv %d, len %d\n", sock_id, len);
	//APP_TRACE("rptr %04X, base %08X\n", rptr, (int)base);
	
	rptr += len;
	var_w5100_sock_reg[sock_id]->rx_read_pointer[0] = rptr >> 8;
	var_w5100_sock_reg[sock_id]->rx_read_pointer[1] = rptr;
	var_w5100_sock_reg[sock_id]->command = W5100_COMMAND_RECV;
	
	return len;
}

/***********************************************************************************************
*	函数名 	: 	BSP_W5100_SockProc
*	功能		:	从接收中断里数据取出来存入接收暂存区sock_proc_buf1，
*					再将接收暂存区的数据存入UMAP
*	参数1		:	
*	参数2		:	
*	注意 默认sock_id 为零
************************************************************************************************/

//接收暂存区
static unsigned char sock_proc_buf1[2048]={0};

void BSP_W5100_SockProc(unsigned char sock_id)
{
	unsigned short len = 0;
	unsigned char int_flag;
	
	int_flag = var_w5100_sock_reg[sock_id]->int_reg;
	var_w5100_sock_reg[sock_id]->int_reg = int_flag;
	
	//APP_TRACE("SockProc %d,%x\r\n", sock_id,int_flag);
	
	//APP_TRACE("int_flag:%02X \n", int_flag);
	
	if(int_flag & W5100_INT_FLAG_SEND_OK)
	{
		
	}
	
	if(int_flag & W5100_INT_FLAG_TIMEOUT)
	{
	}
	
	if(int_flag & W5100_INT_FLAG_RECV)
	{
		 len = BSP_W5100_SockRecv(sock_id,sock_proc_buf1,sizeof(sock_proc_buf1));//从网口缓存中取出放入buf	

		 //u2_printf(" BSP_W5100 rev data \r\n");
		 set_to_table(w5100net1_punmap,sock_proc_buf1,len);//从buf中取出实际的长度放入UNmap		 
		//w5100_is_received = 1;
	}
	
	if(int_flag & W5100_INT_FLAG_DISCON)
	{

	}
	if(int_flag & W5100_INT_FLAG_CONNECT)
	{
	}
}

void BSP_W5100_UDP_Localhost_Config1_set(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,unsigned char sock_id )
{

//延时1ms 可正常请根据自己的硬件设定延时函数
	BSP_Delay(1);
//延时一段时间 才开
//加延时是为了避免丢失改IP 之前的一个数据包
	var_w5100_comm_reg->gateway[0]= 	pudp_localhost_info->localhost_gateway[0];
	var_w5100_comm_reg->gateway[1]= 	pudp_localhost_info->localhost_gateway[1];
	var_w5100_comm_reg->gateway[2]= 	pudp_localhost_info->localhost_gateway[2];
	var_w5100_comm_reg->gateway[3]= 	pudp_localhost_info->localhost_gateway[3];
	var_w5100_comm_reg->subnet[0] =		pudp_localhost_info->localhost_subnet[0];
	var_w5100_comm_reg->subnet[1] =		pudp_localhost_info->localhost_subnet[1];
	var_w5100_comm_reg->subnet[2] =		pudp_localhost_info->localhost_subnet[2];
	var_w5100_comm_reg->subnet[3] =		pudp_localhost_info->localhost_subnet[3];
	var_w5100_comm_reg->src_ip[0] =		pudp_localhost_info->localhost_IP[0];
	var_w5100_comm_reg->src_ip[1] =		pudp_localhost_info->localhost_IP[1];
	var_w5100_comm_reg->src_ip[2] =		pudp_localhost_info->localhost_IP[2];
	var_w5100_comm_reg->src_ip[3] =		pudp_localhost_info->localhost_IP[3];
	var_w5100_comm_reg->src_mac[0]=		pudp_localhost_info->localhost_mac[0];
	var_w5100_comm_reg->src_mac[1]=		pudp_localhost_info->localhost_mac[1];
	var_w5100_comm_reg->src_mac[2]=		pudp_localhost_info->localhost_mac[2];
	var_w5100_comm_reg->src_mac[3]=		pudp_localhost_info->localhost_mac[3];
	var_w5100_comm_reg->src_mac[4]=		pudp_localhost_info->localhost_mac[4];
	var_w5100_comm_reg->src_mac[5]=		pudp_localhost_info->localhost_mac[5];
	
	BSP_W5100_SockInitUdp(0, pudp_localhost_info->localhost_UDPport);
	
}

void BSP_W5100_UDP_Localhost_Config1_read(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,unsigned char sock_id )
{
	unsigned short udp_port=0;
	pudp_localhost_info->localhost_gateway[0] = var_w5100_comm_reg->gateway[0];
	pudp_localhost_info->localhost_gateway[1] =var_w5100_comm_reg->gateway[1];
	pudp_localhost_info->localhost_gateway[2] =var_w5100_comm_reg->gateway[2];
	pudp_localhost_info->localhost_gateway[3] =var_w5100_comm_reg->gateway[3];
	pudp_localhost_info->localhost_subnet[0] =var_w5100_comm_reg->subnet[0];
	pudp_localhost_info->localhost_subnet[1] =var_w5100_comm_reg->subnet[1];
	pudp_localhost_info->localhost_subnet[2] =var_w5100_comm_reg->subnet[2];
	pudp_localhost_info->localhost_subnet[3] =var_w5100_comm_reg->subnet[3];
	pudp_localhost_info->localhost_IP[0] =var_w5100_comm_reg->src_ip[0];
	pudp_localhost_info->localhost_IP[1] =var_w5100_comm_reg->src_ip[1];
	pudp_localhost_info->localhost_IP[2] =var_w5100_comm_reg->src_ip[2];
	pudp_localhost_info->localhost_IP[3] =var_w5100_comm_reg->src_ip[3];
	pudp_localhost_info->localhost_mac[0] =var_w5100_comm_reg->src_mac[0];
	pudp_localhost_info->localhost_mac[1] =var_w5100_comm_reg->src_mac[1];
	pudp_localhost_info->localhost_mac[2] =var_w5100_comm_reg->src_mac[2];
	pudp_localhost_info->localhost_mac[3] =var_w5100_comm_reg->src_mac[3];
	pudp_localhost_info->localhost_mac[4] =var_w5100_comm_reg->src_mac[4];
	pudp_localhost_info->localhost_mac[5] =var_w5100_comm_reg->src_mac[5];
	
	udp_port = (var_w5100_sock_reg[sock_id]->src_port[0]<<8)|(var_w5100_sock_reg[sock_id]->src_port[1]);
	pudp_localhost_info->localhost_UDPport = udp_port;

	
}

unsigned short BSP_W5100_UDP_SEND1(pW5100_UDP_SEND_HEAD udp_send_head,unsigned char sock_id, unsigned char* data, unsigned short len)
{
  int lenth = 0;
  var_w5100_sock_reg[sock_id]->dst_port[0] =udp_send_head->dst_Port[0];
  var_w5100_sock_reg[sock_id]->dst_port[1] =udp_send_head->dst_Port[1];	

  var_w5100_sock_reg[sock_id]->dst_ip[0]  = udp_send_head->dst_IP[0];	
  var_w5100_sock_reg[sock_id]->dst_ip[1]  = udp_send_head->dst_IP[1];	
  var_w5100_sock_reg[sock_id]->dst_ip[2]  = udp_send_head->dst_IP[2];	
  var_w5100_sock_reg[sock_id]->dst_ip[3]  = udp_send_head->dst_IP[3];
  lenth = BSP_W5100_SockSend(sock_id,data,len);
  return lenth;
}
#endif
#ifdef _NET2_W5100_USE_

void BSP_W5100_SockInitUdp2(unsigned char sock_id, unsigned short port)
{
	var_w5100_sock_reg2[sock_id]->mode = 0x20 | W5100_PROTOCOL_UDP;
	var_w5100_sock_reg2[sock_id]->src_port[0] = port >> 8;
	var_w5100_sock_reg2[sock_id]->src_port[1] = port;
	var_w5100_sock_reg2[sock_id]->command = W5100_COMMAND_OPEN;

}

void BSP_W5100_Config2(void)
{
	//只启用sock 0 ,8K缓冲区

	var_w5100_comm_reg2->mode = 0x80;//复位所有寄存器 rst 位会自动清0
	
	var_w5100_comm_reg2->rx_mem_size = 0x03;//8k
	var_w5100_comm_reg2->tx_mem_size = 0x03;//8k

#if 1
	var_w5100_comm_reg2->gateway[0]= g_total_boot_desc_info->udp_local_info2.localhost_gateway[0];
	var_w5100_comm_reg2->gateway[1]= g_total_boot_desc_info->udp_local_info2.localhost_gateway[1];
	var_w5100_comm_reg2->gateway[2]= g_total_boot_desc_info->udp_local_info2.localhost_gateway[2];
	var_w5100_comm_reg2->gateway[3]= g_total_boot_desc_info->udp_local_info2.localhost_gateway[3];
	
	var_w5100_comm_reg2->subnet[0] =	g_total_boot_desc_info->udp_local_info2.localhost_subnet[0];
	var_w5100_comm_reg2->subnet[1] =	g_total_boot_desc_info->udp_local_info2.localhost_subnet[1];
	var_w5100_comm_reg2->subnet[2] =	g_total_boot_desc_info->udp_local_info2.localhost_subnet[2];
	var_w5100_comm_reg2->subnet[3] =	g_total_boot_desc_info->udp_local_info2.localhost_subnet[3];
	
	var_w5100_comm_reg2->src_ip[0] =	g_total_boot_desc_info->udp_local_info2.localhost_IP[0];
	var_w5100_comm_reg2->src_ip[1] =	g_total_boot_desc_info->udp_local_info2.localhost_IP[1];
	var_w5100_comm_reg2->src_ip[2] =	g_total_boot_desc_info->udp_local_info2.localhost_IP[2];
	var_w5100_comm_reg2->src_ip[3] =	g_total_boot_desc_info->udp_local_info2.localhost_IP[3];


		u2_printf("var_w5100_comm_reg2->gateway[0] is %d  \r\n",var_w5100_comm_reg2->gateway[0]);
		u2_printf("var_w5100_comm_reg2->gateway[1] is %d  \r\n",var_w5100_comm_reg2->gateway[1]);
		u2_printf("var_w5100_comm_reg2->gateway[2] is %d  \r\n",var_w5100_comm_reg2->gateway[2]);
		u2_printf("var_w5100_comm_reg2->gateway[3] is %d  \r\n",var_w5100_comm_reg2->gateway[3]);

		u2_printf("var_w5100_comm_reg2->subnet[0] is %d  \r\n",var_w5100_comm_reg2->subnet[0]);
		u2_printf("var_w5100_comm_reg2->subnet[1] is %d  \r\n",var_w5100_comm_reg2->subnet[1]);
		u2_printf("var_w5100_comm_reg2->subnet[2] is %d  \r\n",var_w5100_comm_reg2->subnet[2]);
		u2_printf("var_w5100_comm_reg2->subnet[3] is %d  \r\n",var_w5100_comm_reg2->subnet[3]);

		
		u2_printf("var_w5100_comm_reg2->src_ip[0] is %d  \r\n",var_w5100_comm_reg2->src_ip[0]);
		u2_printf("var_w5100_comm_reg2->src_ip[1] is %d  \r\n",var_w5100_comm_reg2->src_ip[1]);
		u2_printf("var_w5100_comm_reg2->src_ip[2] is %d  \r\n",var_w5100_comm_reg2->src_ip[2]);
		u2_printf("var_w5100_comm_reg2->src_ip[3] is %d  \r\n",var_w5100_comm_reg2->src_ip[3]);
#else
	var_w5100_comm_reg2->gateway[0]= 	192;
	var_w5100_comm_reg2->gateway[1]= 	168;
	var_w5100_comm_reg2->gateway[2]= 	100;
	var_w5100_comm_reg2->gateway[3]= 	1;
	var_w5100_comm_reg2->subnet[0] =	255;
	var_w5100_comm_reg2->subnet[1] =	255;
	var_w5100_comm_reg2->subnet[2] =	255;
	var_w5100_comm_reg2->subnet[3] =	0;
	var_w5100_comm_reg2->src_ip[0] =	192;
	var_w5100_comm_reg2->src_ip[1] =	168;
	var_w5100_comm_reg2->src_ip[2] =	100;
	var_w5100_comm_reg2->src_ip[3] =	29;
#endif	

	var_w5100_comm_reg2->src_mac[0]=	0xc8;
	var_w5100_comm_reg2->src_mac[1]=	0xc7;
	var_w5100_comm_reg2->src_mac[2]=	0xc6;
	var_w5100_comm_reg2->src_mac[3]=	0xc5;
	var_w5100_comm_reg2->src_mac[4]=	0xc4;
	var_w5100_comm_reg2->src_mac[5]=	0xc4;

	
	var_w5100_comm_reg2->int_mask = 	//W5100_INT_MASK_CONFLICT |
								//W5100_INT_MASK_UNREACH	|
								//W5100_INT_MASK_PPPOE	|
								//W5100_INT_MASK_S3_INT	|
								//W5100_INT_MASK_S2_INT	|
								//W5100_INT_MASK_S1_INT	|
								W5100_INT_MASK_S0_INT	;

	#if 1
	BSP_W5100_SockInitUdp2(0,EXCHANGE16BIT(g_total_boot_desc_info->udp_local_info2.localhost_UDPport));	
	#else
	BSP_W5100_SockInitUdp2(0,4000);
	#endif

}




unsigned short BSP_W5100_SockSend2(unsigned char sock_id, unsigned char* data, unsigned short len)
{
	unsigned short l;
	unsigned short wptr;
	unsigned short size;
	unsigned short off;
	unsigned char* base;
	
	wptr = var_w5100_sock_reg2[sock_id]->tx_write_pointer[0];
	wptr = wptr << 8;
	wptr |= var_w5100_sock_reg2[sock_id]->tx_write_pointer[1];
	size = var_w5100_sock_reg2[sock_id]->tx_free_size[0];
	size = size << 8;
	size |= var_w5100_sock_reg2[sock_id]->tx_free_size[1];
	
	if(size == 0)
		return 0;
	
	off = wptr & (W5100_SOCK_TX_MEM_SIZE - 1);
	len = W5100_MIN(len, size);
	l = W5100_MIN(len, W5100_SOCK_TX_MEM_SIZE - off);
	
	base = w5100_tx_mem2 + W5100_SOCK_TX_MEM_SIZE*sock_id;
	BSP_W5100_MemCopy(base + off, data, l);
	BSP_W5100_MemCopy(base + 0, data + l, len - l);
	
	//APP_TRACE("BSP_W5100_SockSend %d, len %d\n", sock_id, len);
	//APP_TRACE("wptr %04X, base %08X\n", wptr, (int)base);
	
	wptr += len;
	var_w5100_sock_reg2[sock_id]->tx_write_pointer[0] = wptr >> 8;
	var_w5100_sock_reg2[sock_id]->tx_write_pointer[1] = wptr;
	var_w5100_sock_reg2[sock_id]->command = W5100_COMMAND_SEND;
	
	return len;
}


unsigned short BSP_W5100_SockRecv2(unsigned char sock_id, unsigned char* data, unsigned short len)
{
	unsigned short l;
	unsigned short rptr;
	unsigned short size;
	unsigned short off;
	unsigned char* base;
	
	rptr = var_w5100_sock_reg2[sock_id]->rx_read_pointer[0];
	rptr = rptr << 8;
	rptr |= var_w5100_sock_reg2[sock_id]->rx_read_pointer[1];
	size = var_w5100_sock_reg2[sock_id]->rx_received_size[0];
	size = size << 8;
	size |= var_w5100_sock_reg2[sock_id]->rx_received_size[1];
	
	if(size == 0)
		return 0;
	
	off = rptr & (W5100_SOCK_RX_MEM_SIZE - 1);
	len = W5100_MIN(len, size);
	l = W5100_MIN(len, W5100_SOCK_RX_MEM_SIZE - off);
	
	base = w5100_rx_mem2 + W5100_SOCK_RX_MEM_SIZE*sock_id;
	BSP_W5100_MemCopy(data, base + off, l);
	BSP_W5100_MemCopy(data + l, base + 0, len - l);

	//APP_TRACE("BSP_W5100_SockRecv %d, len %d\n", sock_id, len);
	//APP_TRACE("rptr %04X, base %08X\n", rptr, (int)base);
	
	rptr += len;
	var_w5100_sock_reg2[sock_id]->rx_read_pointer[0] = rptr >> 8;
	var_w5100_sock_reg2[sock_id]->rx_read_pointer[1] = rptr;
	var_w5100_sock_reg2[sock_id]->command = W5100_COMMAND_RECV;
	
	return len;
}


/***********************************************************************************************
*	函数名 	: 	BSP_W5100_SockProc2
*	功能		:	从接收中断里数据取出来存入接收暂存区sock_proc_buf2，
*					再将接收暂存区的数据存入UMAP
*	参数1		:	
*	参数2		:	
*	注意 默认sock_id 为零
************************************************************************************************/

static unsigned char sock_proc_buf2[2048]={0};

void BSP_W5100_SockProc2(unsigned char sock_id)
{
	unsigned short len = 0;
	unsigned char int_flag;


	int_flag = var_w5100_sock_reg2[sock_id]->int_reg;
	var_w5100_sock_reg2[sock_id]->int_reg = int_flag;
	
	//APP_TRACE("BSP_W5100_SockISR(%d)\n", index);
	//APP_TRACE("int_flag:%02X \n", int_flag);
	
	if(int_flag & W5100_INT_FLAG_SEND_OK)
	{

	}
	if(int_flag & W5100_INT_FLAG_TIMEOUT) {}
	if(int_flag & W5100_INT_FLAG_RECV)
	{
		 len = BSP_W5100_SockRecv2(sock_id,sock_proc_buf2,sizeof(sock_proc_buf2));//从网口缓存中取出放入buf		
		 set_to_table(w5100net2_punmap,sock_proc_buf2,len);//从buf中取出实际的长度放入UNmap
		 
		//w5100_is_received = 1;
	}
	if(int_flag & W5100_INT_FLAG_DISCON) {}
	if(int_flag & W5100_INT_FLAG_CONNECT) {}
}

void BSP_W5100_UDP_Localhost_Config2_set(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,unsigned char sock_id)
{
//延时1ms 可正常请根据自己的硬件设定延时函数
	BSP_Delay(1);
//延时一段时间 才开
//加延时是为了避免丢失改IP 之前的一个数据包
	var_w5100_comm_reg2->gateway[0]= 	pudp_localhost_info->localhost_gateway[0];
	var_w5100_comm_reg2->gateway[1]= 	pudp_localhost_info->localhost_gateway[1];
	var_w5100_comm_reg2->gateway[2]= 	pudp_localhost_info->localhost_gateway[2];
	var_w5100_comm_reg2->gateway[3]= 	pudp_localhost_info->localhost_gateway[3];
	var_w5100_comm_reg2->subnet[0] =	pudp_localhost_info->localhost_subnet[0];
	var_w5100_comm_reg2->subnet[1] =	pudp_localhost_info->localhost_subnet[1];
	var_w5100_comm_reg2->subnet[2] =	pudp_localhost_info->localhost_subnet[2];
	var_w5100_comm_reg2->subnet[3] =	pudp_localhost_info->localhost_subnet[3];
	var_w5100_comm_reg2->src_ip[0] =	pudp_localhost_info->localhost_IP[0];
	var_w5100_comm_reg2->src_ip[1] =	pudp_localhost_info->localhost_IP[1];
	var_w5100_comm_reg2->src_ip[2] =	pudp_localhost_info->localhost_IP[2];
	var_w5100_comm_reg2->src_ip[3] =	pudp_localhost_info->localhost_IP[3];
	var_w5100_comm_reg2->src_mac[0]=	pudp_localhost_info->localhost_mac[0];
	var_w5100_comm_reg2->src_mac[1]=	pudp_localhost_info->localhost_mac[1];
	var_w5100_comm_reg2->src_mac[2]=	pudp_localhost_info->localhost_mac[2];
	var_w5100_comm_reg2->src_mac[3]=	pudp_localhost_info->localhost_mac[3];
	var_w5100_comm_reg2->src_mac[4]=	pudp_localhost_info->localhost_mac[4];
	var_w5100_comm_reg2->src_mac[5]=	pudp_localhost_info->localhost_mac[5];
	
	BSP_W5100_SockInitUdp2(0, pudp_localhost_info->localhost_UDPport);
}


void BSP_W5100_UDP_Localhost_Config2_read(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,unsigned char sock_id )
{
	unsigned short udp_port=0;
	pudp_localhost_info->localhost_gateway[0] = var_w5100_comm_reg2->gateway[0];
	pudp_localhost_info->localhost_gateway[1] =var_w5100_comm_reg2->gateway[1];
	pudp_localhost_info->localhost_gateway[2] =var_w5100_comm_reg2->gateway[2];
	pudp_localhost_info->localhost_gateway[3] =var_w5100_comm_reg2->gateway[3];
	pudp_localhost_info->localhost_subnet[0] =var_w5100_comm_reg2->subnet[0];
	pudp_localhost_info->localhost_subnet[1] =var_w5100_comm_reg2->subnet[1];
	pudp_localhost_info->localhost_subnet[2] =var_w5100_comm_reg2->subnet[2];
	pudp_localhost_info->localhost_subnet[3] =var_w5100_comm_reg2->subnet[3];
	pudp_localhost_info->localhost_IP[0] =var_w5100_comm_reg2->src_ip[0];
	pudp_localhost_info->localhost_IP[1] =var_w5100_comm_reg2->src_ip[1];
	pudp_localhost_info->localhost_IP[2] =var_w5100_comm_reg2->src_ip[2];
	pudp_localhost_info->localhost_IP[3] =var_w5100_comm_reg2->src_ip[3];
	pudp_localhost_info->localhost_mac[0] =var_w5100_comm_reg2->src_mac[0];
	pudp_localhost_info->localhost_mac[1] =var_w5100_comm_reg2->src_mac[1];
	pudp_localhost_info->localhost_mac[2] =var_w5100_comm_reg2->src_mac[2];
	pudp_localhost_info->localhost_mac[3] =var_w5100_comm_reg2->src_mac[3];
	pudp_localhost_info->localhost_mac[4] =var_w5100_comm_reg2->src_mac[4];
	pudp_localhost_info->localhost_mac[5] =var_w5100_comm_reg2->src_mac[5];
	
	udp_port = (var_w5100_sock_reg2[sock_id]->src_port[0]<<8)|(var_w5100_sock_reg2[sock_id]->src_port[1]);
	pudp_localhost_info->localhost_UDPport = udp_port;

	
}


unsigned short BSP_W5100_UDP_SEND2(pW5100_UDP_SEND_HEAD udp_send_head,unsigned char sock_id, unsigned char* data, unsigned short len)
{
  int lenth = 0;
  var_w5100_sock_reg2[sock_id]->dst_port[0] =udp_send_head->dst_Port[0];
  var_w5100_sock_reg2[sock_id]->dst_port[1] =udp_send_head->dst_Port[1];	

  var_w5100_sock_reg2[sock_id]->dst_ip[0]  = udp_send_head->dst_IP[0];	
  var_w5100_sock_reg2[sock_id]->dst_ip[1]  = udp_send_head->dst_IP[1];	
  var_w5100_sock_reg2[sock_id]->dst_ip[2]  = udp_send_head->dst_IP[2];	
  var_w5100_sock_reg2[sock_id]->dst_ip[3]  = udp_send_head->dst_IP[3];	
  lenth = BSP_W5100_SockSend2(sock_id,data,len);
  return lenth;
}

#endif

void BSP_W5100_Config(void)
{
	//rest 共用其实复位一遍就好 为了代码统一性所以复位两遍
#ifdef _NET1_W5100_USE_
	BSP_Delay(10);
	BSP_W5100_rst_clr();
	BSP_Delay(10);
	BSP_W5100_rst_set();
	BSP_Delay(200);
#endif
#ifdef _NET2_W5100_USE_	
	BSP_Delay(10);
	BSP_W5100_rst_clr2();
	BSP_Delay(10);
	BSP_W5100_rst_set2();
	BSP_Delay(200);
#endif

#ifdef _NET1_W5100_USE_
	BSP_W5100_Config1();
#endif
#ifdef _NET2_W5100_USE_
	BSP_W5100_Config2();
#endif
}




//--------------------------------此处以下对外接口------------------------------------//
/***********************************************************************************************
*	函数名 	: 	BSP_W5100_UDP_Localhost_Config_set
*	功能		:	配置本机的UDP IP 等信息
*	参数1		:	pudp_localhost_info :upd 信息结构体
*	参数2		:	chipselect :网口芯片选择
*	注意 默认sock_id 为零
************************************************************************************************/

void BSP_W5100_UDP_Localhost_Config_set(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,CHIPSELECET chipselect)
{
#ifdef _NET1_W5100_USE_
	if(chipselect == NET_FIRST)
	BSP_W5100_UDP_Localhost_Config1_set(pudp_localhost_info,0);
#endif	
#ifdef _NET2_W5100_USE_	
	else if(chipselect == NET_SECEND)
	BSP_W5100_UDP_Localhost_Config2_set(pudp_localhost_info,0);
#endif
}

/***********************************************************************************************
*	函数名 	: 	BSP_W5100_UDP_Localhost_Config_read
*	功能		:	配置本机的UDP IP 等信息
*	参数1		:	pudp_localhost_info :upd 信息结构体
*	参数2		:	chipselect :网口芯片选择
*	注意 默认sock_id 为零
************************************************************************************************/

void BSP_W5100_UDP_Localhost_Config_read(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,CHIPSELECET chipselect)
{
#ifdef _NET1_W5100_USE_
	if(chipselect == NET_FIRST)
	BSP_W5100_UDP_Localhost_Config1_read(pudp_localhost_info,0);
#endif	
#ifdef _NET2_W5100_USE_	
	else if(chipselect == NET_SECEND)
	BSP_W5100_UDP_Localhost_Config2_read(pudp_localhost_info,0);
#endif
}

/***********************************************************************************************
*	函数名 	: 	BSP_W5100_UDP_SEND
*	功能		:	配置本机的UDP IP 等信息
*	参数1		:	udp_send_head :upd 信息结构体
*	参数2		:	chipselect :网口芯片选择
*	参数3		:	data :待发送的数据buf
*	参数4		:	len :待发送的数据长度必须大于0个数据
*	注意 默认sock_id 为零
************************************************************************************************/

unsigned short BSP_W5100_UDP_SEND(pW5100_UDP_SEND_HEAD udp_send_head,CHIPSELECET chipselect, unsigned char* data, unsigned short len)
{
	int active_length = 0;
	if(len<=0)//发送数据必须大于0个 不然网卡中断死掉
	return 0;	
#ifdef _NET1_W5100_USE_	
	if(chipselect == NET_FIRST)
	{
	active_length = BSP_W5100_UDP_SEND1(udp_send_head,0,data,len);
	}
#endif	
#ifdef _NET2_W5100_USE_	
	else if(chipselect == NET_SECEND)
	{
	active_length = BSP_W5100_UDP_SEND2(udp_send_head,0,data,len);
	}
#endif	


return active_length;

}




void BSP_W5100_Init(void)
{

	GPIO_InitTypeDef	GPIO_InitStructure;
	NVIC_InitTypeDef	NVIC_InitStructure;
	EXTI_InitTypeDef	EXTI_InitStructure;
	
#ifdef _NET1_W5100_USE_
	w5100net1_punmap =	create_unmap_table(MAX_UNMAP_CREAT_TABLE,MAX_UNMAP_CREAT_SIZE); //unmap
	RCC_APB2PeriphClockCmd(
		W5100_INT_GPIO_CLK	|
		W5100_RST_GPIO_CLK,
		ENABLE
	);



	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO,ENABLE); //ê1?ü?′ó?1|?üê±?ó


	
	//GPIO   INT               D6
    GPIO_InitStructure.GPIO_Pin = W5100_INT_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(W5100_INT_GPIO_PORT, &GPIO_InitStructure);	

	
	//GPIO            RESET             D3
	GPIO_InitStructure.GPIO_Pin = W5100_RST_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(W5100_RST_GPIO_PORT, &GPIO_InitStructure);

		
	//NVIC
	//NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	NVIC_InitStructure.NVIC_IRQChannel = W5100_INT_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	
	GPIO_EXTILineConfig(W5100_INT_PORT_SOURCE, W5100_INT_PIN_SOURCE);
	
	EXTI_InitStructure.EXTI_Line = W5100_INT_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
    EXTI_ClearITPendingBit(W5100_INT_LINE);


#endif

#ifdef _NET2_W5100_USE_

	w5100net2_punmap =  create_unmap_table(MAX_UNMAP_CREAT_TABLE,MAX_UNMAP_CREAT_SIZE);//unmap 
	RCC_APB2PeriphClockCmd(
		W5100_INT_GPIO_CLK2	|
		W5100_RST_GPIO_CLK2,
		ENABLE
	);

	//GPIO   INT2           E5
    GPIO_InitStructure.GPIO_Pin = W5100_INT_PIN2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(W5100_INT_GPIO_PORT2, &GPIO_InitStructure);	
	//GPIO            RESET             D3
	GPIO_InitStructure.GPIO_Pin = W5100_RST_PIN2;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(W5100_RST_GPIO_PORT2, &GPIO_InitStructure);

	GPIO_EXTILineConfig(W5100_INT_PORT_SOURCE2, W5100_INT_PIN_SOURCE2);
	EXTI_InitStructure.EXTI_Line =W5100_INT_LINE2;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	
	EXTI_ClearITPendingBit(W5100_INT_LINE2);


#endif

	BSP_W5100_Config();
}



void BSP_W5100_ISR(void)
{
	unsigned char int_flag;
	
	//if(BSP_W5100_IntStatus() == Bit_SET)return;
	
	//if(BSP_W5100_IntStatus2() == Bit_SET)return;
	
	//APP_TRACE("W5100_ISR\r\n");
#ifdef _NET1_W5100_USE_
	if(EXTI_GetITStatus(W5100_INT_LINE))
		{
			wcnt++;
		EXTI_ClearITPendingBit(W5100_INT_LINE);
			int_flag = var_w5100_comm_reg->int_reg;
			var_w5100_comm_reg->int_reg = int_flag & 0xF0;

		
			//APP_TRACE("1 int_flag:%02X \n", int_flag);

			var_w5100_comm_reg->int_mask = 0;
			
			
			if(int_flag & W5100_INT_FLAG_CONFLICT) {}
			if(int_flag & W5100_INT_FLAG_UNREACH) {}
			if(int_flag & W5100_INT_FLAG_PPPOE) {}
		//	if(int_flag & W5100_INT_FLAG_S3_INT) { BSP_W5100_SockProc(3); }
		//	if(int_flag & W5100_INT_FLAG_S2_INT) { BSP_W5100_SockProc(2); }
		//	if(int_flag & W5100_INT_FLAG_S1_INT) { BSP_W5100_SockProc(1); }
			if(int_flag & W5100_INT_FLAG_S0_INT) { BSP_W5100_SockProc(0); }
			
			var_w5100_comm_reg->int_mask = W5100_INT_MASK_S0_INT;



			
		}
#endif

#ifdef _NET2_W5100_USE_
	if(EXTI_GetITStatus(W5100_INT_LINE2))
		{
			wcnt++;
		EXTI_ClearITPendingBit(W5100_INT_LINE2);
			int_flag = var_w5100_comm_reg2->int_reg;
			var_w5100_comm_reg2->int_reg = int_flag & 0xF0;

			
			//APP_TRACE("2 int_flag:%02X \r\n", int_flag);
			var_w5100_comm_reg2->int_mask = 0;
                        
			if(int_flag & W5100_INT_FLAG_CONFLICT) {}
			if(int_flag & W5100_INT_FLAG_UNREACH) {}
			if(int_flag & W5100_INT_FLAG_PPPOE) {}
		//	if(int_flag & W5100_INT_FLAG_S3_INT) { BSP_W5100_SockProc2(3); }
		//	if(int_flag & W5100_INT_FLAG_S2_INT) { BSP_W5100_SockProc2(2); }
		//	if(int_flag & W5100_INT_FLAG_S1_INT) { BSP_W5100_SockProc2(1); }
			if(int_flag & W5100_INT_FLAG_S0_INT) { BSP_W5100_SockProc2(0); }
                        
                        var_w5100_comm_reg2->int_mask = W5100_INT_MASK_S0_INT;

		}
#endif

}


void EXTI9_5_IRQHandler(void)
{
	wcnt++;
	
	BSP_W5100_ISR();
}





