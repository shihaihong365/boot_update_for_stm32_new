#include "boot_main.h"
#include "file_pack.h"


char g_total_fie_desc_info_buf[512] = {0};
char g_send_fie_desc_info_buf[512] = {0};

u16 g_total_boot_desc_info_buf[256] = {0};

fie_desc_info_t  * g_total_fie_desc_info  = NULL;
boot_desc_info_t * g_total_boot_desc_info = NULL;

unsigned int g_boot_time_count1 = 0;
unsigned int g_boot_time_count2 = 0;

unsigned int g_wait_update_time_count1 = 0;
unsigned int g_wait_update_time_count2 = 0;

unsigned int g_wait_block_time_count1 = 0;
unsigned int g_wait_block_time_count2 = 0;


unsigned int file_table_len = 0;

get_file_step_e g_get_file_step = GET_FILE_STEP0;

char g_boot_setting_step = 0;
char g_boot_setting_modi_flag = 0;

char g_file_table_creat = 0;
char * file_table_buf = NULL;


boot_desc_info_t g_boot_desc_info;

#define G_UPDATE_FLAG_ON 1
#define G_UPDATE_FLAG_OFF 0

char g_update_flag = G_UPDATE_FLAG_OFF;

//建立文件信息表
char * creat_file_info_talble(fie_desc_info_t * desc_info,unsigned int * len)
{
	unsigned int buf_len = 0;
	char * buf = NULL;

	buf_len = (EXCHANGE32BIT(desc_info->file_size))/(EXCHANGE32BIT(desc_info->buffer_size));
	
	buf_len++;
	
	*len = buf_len;
	
	buf = malloc(buf_len);

	memset(buf,0,buf_len);

	return buf;
}

//搜索文件信息表
int search_file_info_talble(fie_desc_info_t * desc_info, char * buf, unsigned int len, unsigned int * buf_size)
{
	unsigned int i = 0;

	for(i = 0; i < len; i++)
	{
		if(FILE_IS_NOT_GET == buf[i])
		{
			if(i == (len - 1))
			{
				* buf_size = (EXCHANGE32BIT(desc_info->file_size))%(EXCHANGE32BIT(desc_info->buffer_size));
				return i;
			}
			else
			{
				* buf_size = (EXCHANGE32BIT(desc_info->buffer_size));
				return i;
			}
			
		}		
	}

	return FILE_GET_COMPLETE;
	
}

//标记文件信息表
int set_file_info_talble(fie_desc_info_t * desc_info, char * buf, unsigned int len, unsigned int index)
{
	if(index > len)
	{
		return -1;	
	}
	
	if(FILE_IS_NOT_GET == buf[index])
	{			
		buf[index] = FILE_IS_GET;
		
		return index;
	}
	else
	{
		return -1;
	}
}

//检查文件信息表

int check_file_info_talble(fie_desc_info_t * desc_info, char * buf, unsigned int len, unsigned int index)
{
	if(index > len)
	{
		return -1;	
	}
	
	if(FILE_IS_NOT_GET == buf[index])
	{					
		return 0;
	}
	else
	{
		return -1;
	}
}

//获得文件块
int get_file_from_file_info_talble(fie_desc_info_t * desc_info, char * buf, unsigned int len)
{
	
	char cmd_tmp[128];	
	char cmd_out[256];

	file_block_t * obj_point_file_block = NULL;

	unsigned int file_buf_size = 0;

	unsigned int file_block_size = 0;
	
	int file_block_index = 0;

	unsigned int file_name_len = 0;

	unsigned int total_block_pack_size = 0;

	int res = 0;

	unsigned int file_offset = 0;

	u2_printf("this is %s() \r\n",__FUNCTION__);

	file_block_index = search_file_info_talble(desc_info,buf,len,&file_buf_size);

	if(FILE_GET_COMPLETE != file_block_index)
	{
		
		cmd_tmp[0] = 0;

		obj_point_file_block = (file_block_t *)cmd_tmp;

		file_block_size = EXCHANGE32BIT(desc_info->buffer_size);

		obj_point_file_block->size = EXCHANGE32BIT(file_block_size);
		
		file_offset = ((file_block_index*(EXCHANGE32BIT((desc_info->buffer_size)))));
		
		obj_point_file_block->offset = 	EXCHANGE32BIT(file_offset);

		file_name_len = EXCHANGE32BIT(desc_info->name_len);
		
		obj_point_file_block->name_len = EXCHANGE32BIT(file_name_len);

			
		u2_printf("\r\n **** file name is  %s !\r\n",&(desc_info->name));
                u2_printf("\r\n ****  file_offset is  %d !\r\n", file_offset);
               

		memcpy(&(obj_point_file_block->name),&(desc_info->name),EXCHANGE32BIT(desc_info->name_len));
		
		total_block_pack_size = ((sizeof(file_block_t) + file_name_len -1));
			
		obj_point_file_block->total_pack_size = EXCHANGE32BIT(total_block_pack_size);
		
		res = make_pack_5555(METHOD_GET,V_GET_APP_FILE_BLOCK,total_block_pack_size,cmd_tmp,cmd_out);
		
		BSP_W5100_UDP_SEND(&(g_total_boot_desc_info->udp_send_head),NET_FIRST,cmd_out,res);
		BSP_W5100_UDP_SEND(&(g_total_boot_desc_info->udp_send_head),NET_SECEND,cmd_out,res);

	}
	else
	{
		return FILE_GET_COMPLETE;
	}
			
}

//获得文件总信息
int get_file_info_out(void)
{
	#if 0
	W5100_UDP_SEND_HEAD tmp_W5100_UDP_SEND_HEAD;
	#endif
	
	char cmd_tmp[128];	
	char cmd_out[256];

	fie_desc_info_t  * send_desc_info = (fie_desc_info_t *)g_send_fie_desc_info_buf;

	int res = 0;

	u2_printf("this is %s() \r\n",__FUNCTION__);

	u2_printf("ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[0]);
	u2_printf("ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[1]);
	u2_printf("ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[2]);
	u2_printf("ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[3]);
	u2_printf("dst_Port[0] is %x	\r\n",g_total_boot_desc_info->udp_send_head.dst_Port[0]);
	u2_printf("dst_Port[1] is %x	\r\n",g_total_boot_desc_info->udp_send_head.dst_Port[1]);

	u2_printf("name is %s  \r\n",&(g_total_boot_desc_info->update_fie_desc_info.name));

	

	cmd_tmp[0] = 0;

	send_desc_info->total_pack_size = EXCHANGE32BIT(sizeof(fie_desc_info_t) + EXCHANGE32BIT(g_total_boot_desc_info->update_fie_desc_info.name_len) - 1);

	memcpy(&(send_desc_info->name),&(g_total_boot_desc_info->update_fie_desc_info.name),EXCHANGE32BIT(g_total_boot_desc_info->update_fie_desc_info.name_len));
		
	res = make_pack_5555(METHOD_GET,V_GET_APP_FILE_INFO,EXCHANGE32BIT(send_desc_info->total_pack_size),g_send_fie_desc_info_buf,cmd_out);
				
	BSP_W5100_UDP_SEND(&(g_total_boot_desc_info->udp_send_head),NET_FIRST,cmd_out,res);

	BSP_W5100_UDP_SEND(&(g_total_boot_desc_info->udp_send_head),NET_SECEND,cmd_out,res);

	return 0;


}

//读取配置信息
void boot_read_config(void)
{
#if 1
	memset(g_total_boot_desc_info_buf,0,sizeof(g_total_boot_desc_info_buf));
	memset(&g_boot_desc_info,0,sizeof(boot_desc_info_t));
	
	STMFLASH_Read(FLASH_BOOT_CONFIG_ADDR,g_total_boot_desc_info_buf,sizeof(g_total_boot_desc_info_buf)/2);

	memcpy(&g_boot_desc_info,g_total_boot_desc_info_buf,sizeof(boot_desc_info_t));

	g_total_boot_desc_info = (boot_desc_info_t *)g_total_boot_desc_info_buf;
	g_total_fie_desc_info = (fie_desc_info_t *)g_total_fie_desc_info_buf;

	u2_printf("upneme is %s  ... !!\r\n",&(g_total_boot_desc_info->update_fie_desc_info.name));

	u2_printf("ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[0]);
	u2_printf("ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[1]);
	u2_printf("ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[2]);
	u2_printf("ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[3]);


	u2_printf("src_ip[0] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[0]);
	u2_printf("src_ip[1] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[1]);
	u2_printf("src_ip[2] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[2]);
	u2_printf("src_ip[3] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[3]);

	if(1)
	{
		g_total_boot_desc_info->boot_update_app_flag = 1;
		g_total_boot_desc_info->boot_update_config_flag = 1;
		g_total_boot_desc_info->write_flag = 0x5566;	
		g_total_boot_desc_info->udp_send_head.dst_IP[0] = 192;
		g_total_boot_desc_info->udp_send_head.dst_IP[1] = 168;
		g_total_boot_desc_info->udp_send_head.dst_IP[2] = 2;
		g_total_boot_desc_info->udp_send_head.dst_IP[3] = 255;
		g_total_boot_desc_info->udp_send_head.dst_Port[0] = 0x1C;
		g_total_boot_desc_info->udp_send_head.dst_Port[1] = 0x36;


		g_total_boot_desc_info->udp_local_info1.localhost_IP[0] = 192;
		g_total_boot_desc_info->udp_local_info1.localhost_IP[1] = 168;
		g_total_boot_desc_info->udp_local_info1.localhost_IP[2] = 100;
		g_total_boot_desc_info->udp_local_info1.localhost_IP[3] = 28;

		g_total_boot_desc_info->udp_local_info1.localhost_gateway[0] = 192;
		g_total_boot_desc_info->udp_local_info1.localhost_gateway[1] = 168;
		g_total_boot_desc_info->udp_local_info1.localhost_gateway[2] = 2;
		g_total_boot_desc_info->udp_local_info1.localhost_gateway[3] = 1;

		g_total_boot_desc_info->udp_local_info1.localhost_subnet[0] = 255;
		g_total_boot_desc_info->udp_local_info1.localhost_subnet[1] = 255;
		g_total_boot_desc_info->udp_local_info1.localhost_subnet[2] = 255;
		g_total_boot_desc_info->udp_local_info1.localhost_subnet[3] = 0;

		g_total_boot_desc_info->udp_local_info1.localhost_UDPport = EXCHANGE16BIT(4000);

		g_total_boot_desc_info->udp_local_info2.localhost_IP[0] = 192;
		g_total_boot_desc_info->udp_local_info2.localhost_IP[1] = 168;
		g_total_boot_desc_info->udp_local_info2.localhost_IP[2] = 100;
		g_total_boot_desc_info->udp_local_info2.localhost_IP[3] = 29;

		g_total_boot_desc_info->udp_local_info2.localhost_gateway[0] = 192;
		g_total_boot_desc_info->udp_local_info2.localhost_gateway[1] = 168;
		g_total_boot_desc_info->udp_local_info2.localhost_gateway[2] = 2;
		g_total_boot_desc_info->udp_local_info2.localhost_gateway[3] = 1;

		g_total_boot_desc_info->udp_local_info2.localhost_subnet[0] = 255;
		g_total_boot_desc_info->udp_local_info2.localhost_subnet[1] = 255;
		g_total_boot_desc_info->udp_local_info2.localhost_subnet[2] = 255;
		g_total_boot_desc_info->udp_local_info2.localhost_subnet[3] = 0;

		g_total_boot_desc_info->udp_local_info2.localhost_UDPport = EXCHANGE16BIT(4000);

		memcpy(&(g_total_boot_desc_info->update_fie_desc_info.name),"app.bin",strlen("app.bin"));

	}

#else
	memset(g_total_boot_desc_info_buf,0,sizeof(g_total_boot_desc_info_buf));
	memset(&g_boot_desc_info,0,sizeof(boot_desc_info_t));
	
	STMFLASH_Read(FLASH_BOOT_CONFIG_ADDR,g_total_boot_desc_info_buf,sizeof(g_total_boot_desc_info_buf)/2);

	memcpy(&g_boot_desc_info,g_total_boot_desc_info_buf,sizeof(boot_desc_info_t));

	g_total_boot_desc_info = (boot_desc_info_t *)g_total_boot_desc_info_buf;
	g_total_fie_desc_info = (fie_desc_info_t *)g_total_fie_desc_info_buf;

	u2_printf("upneme is %s  ... !!\r\n",&(g_total_boot_desc_info->update_fie_desc_info.name));

	u2_printf("ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[0]);
	u2_printf("ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[1]);
	u2_printf("ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[2]);
	u2_printf("ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[3]);


	u2_printf("src_ip[0] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[0]);
	u2_printf("src_ip[1] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[1]);
	u2_printf("src_ip[2] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[2]);
	u2_printf("src_ip[3] is %d  \r\n",g_total_boot_desc_info->udp_local_info1.localhost_IP[3]);

	if(g_total_boot_desc_info->write_flag != 0x5566)
	{
		g_total_boot_desc_info->boot_update_app_flag = 1;
		g_total_boot_desc_info->boot_update_config_flag = 1;
		g_total_boot_desc_info->write_flag = 0x5566;	
		g_total_boot_desc_info->udp_send_head.dst_IP[0] = 192;
		g_total_boot_desc_info->udp_send_head.dst_IP[1] = 168;
		g_total_boot_desc_info->udp_send_head.dst_IP[2] = 2;
		g_total_boot_desc_info->udp_send_head.dst_IP[3] = 255;
		g_total_boot_desc_info->udp_send_head.dst_Port[0] = 0x1C;
		g_total_boot_desc_info->udp_send_head.dst_Port[1] = 0x36;


		g_total_boot_desc_info->udp_local_info1.localhost_IP[0] = 192;
		g_total_boot_desc_info->udp_local_info1.localhost_IP[1] = 168;
		g_total_boot_desc_info->udp_local_info1.localhost_IP[2] = 2;
		g_total_boot_desc_info->udp_local_info1.localhost_IP[3] = 28;

		g_total_boot_desc_info->udp_local_info1.localhost_gateway[0] = 192;
		g_total_boot_desc_info->udp_local_info1.localhost_gateway[1] = 168;
		g_total_boot_desc_info->udp_local_info1.localhost_gateway[2] = 2;
		g_total_boot_desc_info->udp_local_info1.localhost_gateway[3] = 1;

		g_total_boot_desc_info->udp_local_info1.localhost_subnet[0] = 255;
		g_total_boot_desc_info->udp_local_info1.localhost_subnet[1] = 255;
		g_total_boot_desc_info->udp_local_info1.localhost_subnet[2] = 255;
		g_total_boot_desc_info->udp_local_info1.localhost_subnet[3] = 0;

		g_total_boot_desc_info->udp_local_info1.localhost_UDPport = EXCHANGE16BIT(4000);

		g_total_boot_desc_info->udp_local_info2.localhost_IP[0] = 192;
		g_total_boot_desc_info->udp_local_info2.localhost_IP[1] = 168;
		g_total_boot_desc_info->udp_local_info2.localhost_IP[2] = 2;
		g_total_boot_desc_info->udp_local_info2.localhost_IP[3] = 29;

		g_total_boot_desc_info->udp_local_info2.localhost_gateway[0] = 192;
		g_total_boot_desc_info->udp_local_info2.localhost_gateway[1] = 168;
		g_total_boot_desc_info->udp_local_info2.localhost_gateway[2] = 2;
		g_total_boot_desc_info->udp_local_info2.localhost_gateway[3] = 1;

		g_total_boot_desc_info->udp_local_info2.localhost_subnet[0] = 255;
		g_total_boot_desc_info->udp_local_info2.localhost_subnet[1] = 255;
		g_total_boot_desc_info->udp_local_info2.localhost_subnet[2] = 255;
		g_total_boot_desc_info->udp_local_info2.localhost_subnet[3] = 0;

		g_total_boot_desc_info->udp_local_info2.localhost_UDPport = EXCHANGE16BIT(4000);

		memcpy(&(g_total_boot_desc_info->update_fie_desc_info.name),"app.bin",strlen("app.bin"));

	}	
#endif	
}


int boot_main(void)
{
	int i = 0;
	int res = 0;
	unsigned char w5100_rev_file_buf[2048] = {0};

	//W5100_UDP_SEND_HEAD tmp_W5100_UDP_SEND_HEAD;
	
	char cmd_tmp[24];
	char cmd_out[164];
	unsigned char * cmd_buf = NULL;

	SystemInit();
	
	bsp_intit_usart2();
	
	BSP_FSMC_Init();

	
#if 1
	boot_read_config();
#endif
	
	BSP_W5100_Init();
		
	BSP_SFLASH_Init();
	
//////////////////////////////////////////////////////////////////////////////////


	while(1)
	{	

		res = get_from_table(w5100net1_punmap,w5100_rev_file_buf,MAX_UNMAP_CREAT_SIZE);
		
		if(res > 0)
		{

			u2_printf("rev %d byte !!! \r\n",res);	

			cmd_buf = (unsigned char * )(w5100_rev_file_buf + sizeof(udphdr_t));
					
			make_unpack_5555_Process(cmd_buf,0);			
		}

		res = get_from_table(w5100net2_punmap,w5100_rev_file_buf,MAX_UNMAP_CREAT_SIZE);
		
		if(res > 0)
		{

			u2_printf("rev %d byte !!! \r\n",res);	

			cmd_buf = (unsigned char * )(w5100_rev_file_buf + sizeof(udphdr_t));
					
			make_unpack_5555_Process(cmd_buf,0);			
		}

		if(G_UPDATE_FLAG_ON == g_update_flag)
		{	
			if(GET_FILE_STEP0 == g_get_file_step)
			{
							
				if(g_boot_time_count1++ > 64000*2)
				{
					g_boot_time_count1 = 0;

					u2_printf("sys wait for update !!\r\n");
					
					if(g_boot_time_count2++ > MAX_BOOT_TIME_COUNT )
					{
						//等待文件信息头超时
						break;
					}				
				}
				
				get_file_info_out();

			}
			else if(GET_FILE_STEP1 == g_get_file_step)
			{

				g_total_fie_desc_info = (fie_desc_info_t *)g_total_fie_desc_info_buf;
				
				if(0 == g_file_table_creat)
				{							
					file_table_buf = creat_file_info_talble(g_total_fie_desc_info,&file_table_len);
					g_file_table_creat = 1;
				}
							
				if(FILE_GET_COMPLETE == get_file_from_file_info_talble(g_total_fie_desc_info,file_table_buf,file_table_len))
				{
					u2_printf("rev FILE_GET_COMPLETE !!! ");
					break;			
				}

				if(g_wait_block_time_count1++ > 64000*2)
				{
					g_wait_block_time_count1 = 0;
					
					if(g_wait_block_time_count1++ > MAX_UPDATE_BLOCK_TIME_COUNT )
					{
						//等待获得文件块超时
						break;
					}				
				}
										
			}
		}
		else
		{

			if(g_wait_update_time_count1++ > 64000*2)
			{
				g_wait_update_time_count1 = 0;

				u2_printf("sys wait for config !!\r\n");
				
				if(g_wait_update_time_count2++ > MAX_UPDATE_TIME_COUNT )
				{
					//等待获得文配置信息头超时
					break;
				}				
			}

		}
		                								
	}

	if(((*(vu32*)(FLASH_APP1_ADDR+4))&0xFF000000)==0x08000000)//判断是否为0X08XXXXXX.
	{	 
		u2_printf("load ... !!\r\n");
		BSP_Delay(3800);
		iap_load_app(FLASH_APP1_ADDR);//执行FLASH APP代码
	}

	while(1)
	{
		//app不存在
		u2_printf("can not load !!\r\n");
		BSP_Delay(800);
	}
	
}













