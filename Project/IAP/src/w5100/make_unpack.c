#include "make_unpack.h"

unsigned char make_unpack_5555_Process(unsigned char *buf, unsigned int len)
{
	fie_desc_info_t * fie_desc_info_point = NULL;
	int i = 0;

	file_block_t * obj_point_file_block =  NULL;
	file_data_t * obj_point_file_data = NULL;
	boot_desc_info_t * obj_point_boot_desc_info = NULL;

	unsigned int file_len = 0;
	unsigned int total_file_data_len = 0;
	int file_block_index = 0;
	unsigned int file_total_pack_size = 0;

	unsigned int file_offset = 0;

	unsigned int falsh_file_offset = 0;
	
	unsigned int file_buffer_size = 0;
		
	//更新MB结构体 
	switch(((pPack_5555_head_t)(buf))->method)
	{
		case METHOD_RETURN_GET:
			switch(EXCHANGE16BIT(((pPack_5555_head_t)(buf))->cmd))
            {
				case CMD_GLOBAL_ALL:
					  break;
				//获取文件总信息	  
			  case V_GET_APP_FILE_INFO:
				
				fie_desc_info_point = (fie_desc_info_t *)(buf + sizeof(pack_5555_head_t));

				g_total_fie_desc_info = (fie_desc_info_t *)g_total_fie_desc_info_buf;

				memcpy((char *)g_total_fie_desc_info,fie_desc_info_point,EXCHANGE32BIT(fie_desc_info_point->total_pack_size));
				
				g_get_file_step = 1;
					
				break;
				//获取文件块
				case V_GET_APP_FILE_BLOCK:
				
				if(GET_FILE_STEP1 == g_get_file_step)
				{
					obj_point_file_block =  (file_block_t *)(buf + sizeof(pack_5555_head_t));

					if( (0 == memcmp((&g_total_fie_desc_info->name),(&obj_point_file_block->name),EXCHANGE32BIT(g_total_fie_desc_info->name_len)))&&  \
						(g_total_fie_desc_info->file_size) == obj_point_file_block->file_size )
					{
						file_offset = ((EXCHANGE32BIT(obj_point_file_block->offset)));
						file_buffer_size = (EXCHANGE32BIT((g_total_fie_desc_info->buffer_size)));

						file_block_index = file_offset/file_buffer_size;
									
						file_total_pack_size = EXCHANGE32BIT(obj_point_file_block->total_pack_size);

						obj_point_file_data = (file_data_t * )(buf + sizeof(pack_5555_head_t)+(file_total_pack_size));

						total_file_data_len = EXCHANGE32BIT(obj_point_file_data->file_data_len);

						falsh_file_offset = file_block_index*(EXCHANGE32BIT(g_total_fie_desc_info->buffer_size));

						#if 1
						u2_printf("------ file_block->name is  %s !------\r\n",&obj_point_file_block->name);
        				u2_printf("------ falsh_file_offset is  %d !-----\r\n", falsh_file_offset);
						#endif
						
						if(0 == check_file_info_talble(g_total_fie_desc_info,file_table_buf,file_table_len,file_block_index))
						{
							iap_write_appbin(FLASH_APP1_ADDR + (falsh_file_offset),(u8 *)(&obj_point_file_data->data_buff),total_file_data_len);//更新FLASH代码
								
							set_file_info_talble(g_total_fie_desc_info,file_table_buf,file_table_len,file_block_index);	
						}
					}					
				}
					
					break;
				
			
                         default:
		                break;
         }
						
		break;
		case METHOD_RETURN_SET:
			
		break;

		case METHOD_SET:
		switch(EXCHANGE16BIT(((pPack_5555_head_t)(buf))->cmd))
		{		//设置配置信息
			case V_SET_BOOT_CONFIG_INFO:

				   u2_printf("sys wait V_SET_BOOT_CONFIG_INFO 1111 !!\r\n");
					
	 			  obj_point_boot_desc_info = (boot_desc_info_t *)(buf + sizeof(pack_5555_head_t));
	 			  
	 			  if(obj_point_boot_desc_info->boot_update_config_flag == 1)
	 			  {	
				  	
				  	g_total_boot_desc_info = (boot_desc_info_t *)g_total_boot_desc_info_buf;
					g_total_fie_desc_info = (fie_desc_info_t *)g_total_fie_desc_info_buf;

					memcpy(g_total_boot_desc_info,obj_point_boot_desc_info,EXCHANGE32BIT(obj_point_boot_desc_info->total_pack_size));
					
	 			  	memcpy(&g_boot_desc_info,obj_point_boot_desc_info,EXCHANGE32BIT(obj_point_boot_desc_info->total_pack_size));

					memcpy(&(g_total_fie_desc_info->name),&(g_total_boot_desc_info->update_fie_desc_info.name),EXCHANGE32BIT(g_total_boot_desc_info->update_fie_desc_info.name_len));

					 u2_printf("sys update name is %s !!\r\n",&(g_total_boot_desc_info->update_fie_desc_info.name));

					 u2_printf("***ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[0]);
					 u2_printf("***ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[1]);
					 u2_printf("***ip[0] is %d  \r\n",g_total_boot_desc_info->udp_send_head.dst_IP[2]);
					 u2_printf("***ip[0] is %d	\r\n",g_total_boot_desc_info->udp_send_head.dst_IP[3]);
					
	 				iap_write_appbin(FLASH_BOOT_CONFIG_ADDR,(u8 *)obj_point_boot_desc_info,EXCHANGE32BIT(obj_point_boot_desc_info->total_pack_size));
	 
	 			  }
	 
	 			  if(obj_point_boot_desc_info->boot_update_app_flag == 1)
	 			  {
				  	//如果需要升级文件
				  		u2_printf("sys wait V_SET_BOOT_CONFIG_INFO 222 !!\r\n");
	 			 	   g_update_flag = G_UPDATE_FLAG_ON;
	 			  }	
				  else
				  {		//如果不需要升级文件 重启
				  		u2_printf("reset sys !!!!!! \r\n");
				  		__set_FAULTMASK(1);  
						NVIC_SystemReset();
				  }
	 			  break;
			 default:
		          break;
		}
		break;
		
		default:
		break;

	}
   return 0;

}

