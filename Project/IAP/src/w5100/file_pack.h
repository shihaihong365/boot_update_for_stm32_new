#ifndef __FILE_PACK_H__
#define __FILE_PACK_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "bsp_w5100.h"

#define  V_SET_BOOT_CONFIG_INFO   0x0920
#define  V_GET_APP_FILE_INFO      0x0921
#define  V_GET_APP_FILE_BLOCK 	  0x0922


#define FILE_IS_NOT_GET 0
#define FILE_IS_GET 1
#define FILE_GET_COMPLETE -1

#pragma pack(1)

typedef struct fie_block
{	 
	unsigned int offset;
	unsigned int size;
	unsigned int total_pack_size;
	unsigned int file_size;
	unsigned int name_len;
	char name;
}file_block_t;

typedef struct file_data
{	 
	unsigned int file_data_len;
	char data_buff;
}file_data_t;

typedef struct fie_desc_info
{	 
	unsigned int buffer_size;
	unsigned int file_size;
	unsigned int offset;
	unsigned int size;	
	unsigned int total_pack_size;

	unsigned int name_len;
	char name;

}fie_desc_info_t;

typedef struct boot_desc_info  //boot 配置信息描述
{	 
	unsigned int  write_flag;
	char boot_update_app_flag;      //是否升级app标志
	char boot_update_config_flag;   //是否升级配置标志
	W5100_UDP_SEND_HEAD udp_send_head;//远程升级ip信息
	W5100_UDP_SEND_HEAD udp_local_head;
	W5100_UDP_LOCALHOST_INFO udp_local_info1;  //本地网口1配置
	W5100_UDP_LOCALHOST_INFO udp_local_info2;  //本地网口2配置

	unsigned int total_pack_size;             //整个包大小，包括文件名的长度
	fie_desc_info_t update_fie_desc_info;      //升级文件名在里面，注意要为其分配内存

}boot_desc_info_t;

typedef struct udphdr 
{
	unsigned short   source;
	unsigned short	dest;
	unsigned short	len;
	unsigned short	check;
}udphdr_t;

typedef enum get_file_step_type
{
	GET_FILE_STEP0 = 0,
	GET_FILE_STEP1,	
}get_file_step_e;

#pragma pack()

#ifdef __cplusplus
}
#endif

#endif


