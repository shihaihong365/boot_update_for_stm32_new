#ifndef __BOOT_MAIN_H__
#define __BOOT_MAIN_H__

#ifdef __cplusplus
extern "C" {
#endif

#include "delay.h"
#include "sys.h"
#include "usart.h"
#include "stmflash.h"
#include "iap.h"
#include "usart2.h"
#include "app_file_table.h"
#include "bsp_w5100.h"
#include "bsp_fsmc.h"
#include "file_pack.h"
#include "make_pack.h"
#include "make_unpack.h"

#define MAX_BOOT_TIME_COUNT 13
#define MAX_UPDATE_TIME_COUNT 13
#define MAX_UPDATE_BLOCK_TIME_COUNT 503

#define G_UPDATE_FLAG_ON 1
#define G_UPDATE_FLAG_OFF 0

extern fie_desc_info_t * g_total_fie_desc_info;

extern char g_total_fie_desc_info_buf[512];
extern char * file_table_buf;
extern char g_get_file_step;

extern boot_desc_info_t g_boot_desc_info;
extern boot_desc_info_t * g_total_boot_desc_info;

extern u16 g_total_boot_desc_info_buf[256];

extern char g_update_flag;

extern unsigned int file_table_len;

extern int set_file_info_talble(fie_desc_info_t * desc_info, char * buf, unsigned int len, unsigned int index);

extern int check_file_info_talble(fie_desc_info_t * desc_info, char * buf, unsigned int len, unsigned int index);


#ifdef __cplusplus
}
#endif

#endif