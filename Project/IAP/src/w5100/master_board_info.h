

#ifndef __MASTER_BOARD_INFO_
#define __MASTER_BOARD_INFO_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <stdio.h>
#include "string.h"
#include <stdarg.h>
#include <stdlib.h>
#pragma pack (1)
//功能模块选择宏定义
#define __FUCTION_GLOBAL_CONFIG__
#define __FUCTION_SCREEN_SWITCH__
#define __FUCTION_BOARDCARD_ONLINE__
#define __FUCTION_BOARDCARD_INFO__
#define __FUCTION_SENCE_SWITCH__

//配置信息
enum
{
	MB_SYS_CONFIG_FLAG		= 0xEB9055AA,   //
	MB_HARDWARE_VERSION 		= 0x0101,       // 
	MB_FIRMWARE_VERSION 		= 0x0100,       //
	MB_GLOBALE_LANGUAGE	 =0x01,// 1 english 0 chinese
	COM_NUMBER 			=0x04,
	NET_NUMBER 			=0x02,

	MB_HARDWARE_INPORT		= 18,
	MB_HARDWARE_OUTPORT		= 18,
#ifdef 	__FUCTION_SCREEN_SWITCH__
	SCREEN_SWITCH_INPORT		= 18,
	SCREEN_SWITCH_OUTPORT		= 18,
#endif
#ifdef 	__FUCTION_BOARDCARD_ONLINE__
	BOARDCARD_ONLINE_INPORT		= 18,
	BOARDCARD_ONLINE_OUTPORT	= 18,
#endif	
#ifdef 	__FUCTION_BOARDCARD_INFO__
	BOARDCARD_INFO_INPORT		= 18,
	BOARDCARD_INFO_OUTPORT		= 18,
#endif	
#ifdef 	__FUCTION_SENCE_SWITCH__
	SENCE_SWITCH_NUMBER		= 16,
#endif	

//此处添加更多的IN OUT

};
//---------命令索引号-------//
//命令
enum
{
	MB_GET 				= 0x01,//请求获取内容
	MB_SET 				= 0x02,//设置内容
	MB_RETURN_GET		= 0x11,//返回请求内容
	MB_RETURN_SET 		= 0x12,//返回设置结果
	MB_EVENT			= 0x05,//从设备有变化主动通知主设备
};

enum
{
	CONFIG_COM1 = 0x01,
	CONFIG_COM2 = 0x02,
	CONFIG_COM3 = 0x03,
	CONFIG_COM4 = 0x04,
};

enum
{
	CONFIG_NET1 = 0x81,
	CONFIG_NET2 = 0x82,
};

//板卡ID 0x01-0x18 十进制计数

//---------命令索引号-------//



typedef struct _MB_global_config_com_
{
	unsigned int	global_baudrate;				//串口波特率
	unsigned char	global_datepaity;				//串口数据校验位
	unsigned char	global_datebit;					//串口数据长度
	unsigned char	global_stopbit;					//串口停止位

}MB_global_config_com_t,*pMB_global_config_com_t;

typedef struct _MB_global_config_net_
{
	unsigned char	global_gateway[4];				//网口网关
	unsigned char	global_subnet[4];				//网口子网掩码
	unsigned char	global_src_ip[4];				//网口IP
	unsigned char	global_src_mac[6];				//网口MAC地址
	unsigned short	global_udp_port;				//UDP通讯端口号
	
}MB_global_config_net_t,*pMB_global_config_net_t;

#ifdef 	__FUCTION_GLOBAL_CONFIG__

typedef struct _MB_global_config_
{
	unsigned int	syc_config_flag;				//配置同步标志
	unsigned char	global_inport;					//硬件输入端口数
	unsigned char	global_outport;					//硬件输出端口数
	unsigned char   global_version[4];				//软硬件输出端口数高2byte为硬件版本号.低2byte为软件版本号
	unsigned char	global_sn[4];					//产品序列号
	unsigned int	global_startcouter;				//开机次数
	unsigned char   global_buzzer;					//蜂鸣器标志
	unsigned int	global_temperature;				//温度
	unsigned char	global_language;				//语言
 	pMB_global_config_com_t global_config_com[COM_NUMBER];		//串口配置数组
	pMB_global_config_net_t global_config_net[NET_NUMBER];		//网口配置数组

}MB_global_config_t,*pMB_global_config_t;
#endif

#ifdef 	__FUCTION_SCREEN_SWITCH__

typedef struct _MB_screen_switch_
{
	unsigned int	syc_config_flag;				//配置同步标志
	unsigned char	screen_switch_inport;			//视频切换输入路数
	unsigned char	screen_switch_outport;			//视频切换输出路数

	unsigned char*  screen_switch_status;			//视频切换状态表长度由输出路数决定
													//:值代表输入(0代表关闭 F0 代表不切)，下标代表输出

	
}MB_screen_switch_t,*pMB_screen_switch_t;

#endif

//板卡在线配置
#ifdef 	__FUCTION_BOARDCARD_ONLINE__	
typedef struct _MB_boardcard_online_
{
	unsigned int	syc_config_flag;			//配置同步标志
	unsigned char	boardcard_online_inport;		//板卡在线输入路数
	unsigned char	boardcard_online_outport;		//板卡在线输出路数

	unsigned char*  boardcard_online_status;		//板卡在线状态表
													//长度由输入输出共同决定，
													//输入占有比例由boardcard_online_inport/(boardcard_online_inport+boardcard_online_outport)决定
													// 1表示在线 0表示不在线
	
}MB_boardcard_online_t,*pMB_boardcard_online_t;

#endif

#ifdef 	__FUCTION_BOARDCARD_INFO__
typedef struct _MB_boardcard_info_
{
	unsigned int	syc_config_flag;			//配置同步标志
	unsigned char	boardcard_info_inport;			//板卡信息输入路数
	unsigned char	boardcard_info_outport;			//板卡信息输出路数
	unsigned short    boardcard_info_ID;			//板卡位号
													//高位作为输入输出端的标识0表示输入端 1表示输出端
													//低BYTE表示输入输出位号
	
}MB_boardcard_info_t,*pMB_boardcard_info_t;
#endif

#ifdef 	__FUCTION_SENCE_SWITCH__
typedef struct _MB_sence_switch_
{
	unsigned int	syc_config_flag;			//配置同步标志
	unsigned char	sence_switch_number;		//场景路数
	unsigned char*  sence_ID[SENCE_SWITCH_NUMBER];//场景表 0代表当前场景
	
}MB_sence_switch_t,*pMB_sence_switch_t;
#endif

//总结构体
typedef struct _MB_info_
{
	unsigned int	syc_config_flag;			//配置同步标志
	
	pMB_global_config_t		MB_global_config;	//全局配置
#ifdef 	__FUCTION_SCREEN_SWITCH__	
	pMB_screen_switch_t		screen_switch;		//视频切换配置
#endif	
#ifdef 	__FUCTION_BOARDCARD_ONLINE__	
	pMB_boardcard_online_t	boardcard_online;		//板卡在线配置
#endif
#ifdef 	__FUCTION_BOARDCARD_INFO__	
	pMB_boardcard_info_t   	boardcard_info;			//板卡查询info 配置
#endif
#ifdef 	__FUCTION_SENCE_SWITCH__	
	pMB_sence_switch_t   	sence_switch;			//场景配置
#endif
		
}MB_info_t,*pMB_info_t;





extern pMB_info_t pCurrent_MB_info;


pMB_info_t malloc_MB_info(void);

int MB_CONFIG_INIT(void);




#pragma pack ()
#ifdef __cplusplus
}
#endif

#endif



