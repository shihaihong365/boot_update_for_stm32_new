#ifdef __cplusplus
extern "C" {
#endif
#include "single_board_info.h"

#pragma pack (1)

//板卡结构体数组的输入输出分配 由SB_HARDWARE_INPORT:SB_HARDWARE_OUTPORT 比例分配
pSB_info_t pCurrent_SB_info[SB_HARDWARE_INPORT+SB_HARDWARE_OUTPORT]={NULL};

#ifdef SB_DEBUG	
static int SB_info_malloc_size = 0;//调试用
#endif

/********************************************************************************************************
*	函数名:  	pSB_info_t malloc_SB_info(void)
*	功能	   : 	开辟板卡结构体空间
*	返回值:	板卡信息结构体指针
*
*********************************************************************************************************/

pSB_info_t malloc_SB_info(void)
{
	pSB_info_t pSB_info = NULL;

//板卡head	
	pSB_info = (pSB_info_t)malloc(sizeof(SB_info_t));
	memset(pSB_info,0,sizeof(SB_info_t));
	pSB_info->sb_syc_config_flag = SB_SYC_CONFIG_FLAG;
	pSB_info->sb_info_inport	= SB_HARDWARE_INPORT;
	pSB_info->sb_info_outport	= SB_HARDWARE_INPORT;
#ifdef SB_DEBUG	
	SB_info_malloc_size += sizeof(SB_info_t);
#endif

#ifdef	__FUNCTION_VIDEO_IN_INFO__
//板卡视频输出端信息	
	pSB_info->video_out_info = (pSB_video_out_info_t)malloc(sizeof(SB_video_out_info_t));
	memset(pSB_info->video_out_info,0,sizeof(SB_video_out_info_t));
#ifdef SB_DEBUG		
	SB_info_malloc_size += sizeof(SB_video_out_info_t);
#endif

#endif

#ifdef	__FUNCTION_VIDEO_OUT_INFO__
//板卡视频输入端信息	
	pSB_info->video_in_info = (pSB_video_in_info_t)malloc(sizeof(SB_video_in_info_t));
	memset(pSB_info->video_out_info,0,sizeof(SB_video_out_info_t));
#ifdef SB_DEBUG		
	SB_info_malloc_size += sizeof(SB_video_in_info_t);
#endif

#endif

#ifdef	__FUNCTION_AUDIO_IN_INFO__	
//板卡音频输入端信息	
	pSB_info->audio_in_info= (pSB_audio_in_info_t)malloc(sizeof(SB_audio_in_info_t));
	memset(pSB_info->audio_in_info,0,sizeof(SB_audio_in_info_t));
#ifdef SB_DEBUG	
	SB_info_malloc_size += sizeof(SB_audio_in_info_t);
#endif

#endif

#ifdef __FUNCTION_BASIC_INFO__
//板卡基本信息	
	pSB_info->basic_info= (pSB_basic_info_t)malloc(sizeof(SB_basic_info_t));
	memset(pSB_info->basic_info,0,sizeof(SB_basic_info_t));
#ifdef SB_DEBUG	
	SB_info_malloc_size += sizeof(SB_basic_info_t);
#endif

#endif

#ifdef __FUNCTION_CONTORL_INFO__	
//板卡控制信息
	pSB_info->contorl_info = (pSB_contorl_info_t)malloc(sizeof(SB_contorl_info_t));
	memset(pSB_info->basic_info,0,sizeof(SB_contorl_info_t));
#ifdef SB_DEBUG	
	SB_info_malloc_size += sizeof(SB_contorl_info_t);
#endif

#endif

//--------如有新的功能--在此添加新的功能项---注意宏定义---//


//--------如有新的功能--在此添加新的功能项---注意宏定义---//
#ifdef SB_DEBUG	
	APP_TRACE("一块板卡 MALLOC 的空间大小为 %d " ,SB_info_malloc_size);
#endif

        return pSB_info;

}

/********************************************************************************************************
*	函数名:  	void  memset_SB_info(pSB_info_t pSB_info)
*	功能	   : 	清0 板卡结构体空间,不清0 头指针，不释放空间
*	返回值:  	无
*	参数	   :	pSB_info_t类型的结构体指针即待清理的结构体
*********************************************************************************************************/

void  memset_SB_info(pSB_info_t pSB_info)
{

#ifdef __FUNCTION_VIDEO_IN_INFO__
	memset(pSB_info->video_out_info,0,sizeof(SB_video_out_info_t));
#endif

#ifdef __FUNCTION_VIDEO_OUT_INFO__
	memset(pSB_info->video_out_info,0,sizeof(SB_video_out_info_t));
#endif

#ifdef __FUNCTION_AUDIO_IN_INFO__
	memset(pSB_info->audio_in_info,0,sizeof(SB_audio_in_info_t));
#endif

#ifdef __FUNCTION_BASIC_INFO__
	memset(pSB_info->basic_info,0,sizeof(SB_basic_info_t));
#endif

#ifdef __FUNCTION_CONTORL_INFO__
	memset(pSB_info->basic_info,0,sizeof(SB_contorl_info_t));
#endif
//--------如有新的功能--在此添加新的清理项---注意宏定义---//


//--------如有新的功能--在此添加新的清理项---注意宏定义---//

}


/********************************************************************************************************
*	函数名:  	int SB_CONFIG_INIT(void)
*	功能	   : 	初始化所有板卡结构体
*	返回值:	板卡信息结构体指针数组return 1 成功
*
*********************************************************************************************************/

int SB_CONFIG_INIT(void)
{
	int i=0;
#ifdef SB_DEBUG	
	int total_malloc_size = 0;
#endif
	for(i=0;i<SB_HARDWARE_INPORT+SB_HARDWARE_OUTPORT;i++)
	{ 
#ifdef SB_DEBUG
	  SB_info_malloc_size = 0;
#endif

	  pCurrent_SB_info[i] = malloc_SB_info();

#ifdef SB_DEBUG
	  total_malloc_size += SB_info_malloc_size;
#endif
	}
#ifdef SB_DEBUG	
		APP_TRACE("总的 MALLOC 的空间大小为: %d ",total_malloc_size);
#endif  

        return 1;

}

#pragma pack ()

#ifdef __cplusplus
}
#endif


