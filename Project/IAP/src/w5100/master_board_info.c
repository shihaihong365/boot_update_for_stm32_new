#ifdef __cplusplus
extern "C" {
#endif
#include "master_board_info.h"

#pragma pack (1)

int k;
pMB_global_config_net_t j;

pMB_info_t pCurrent_MB_info = NULL;

#ifdef MB_DEBUG
static int MB_malloc_size = 0;
#endif  

/********************************************************************************************************
*	函数名:  	pMB_info_t malloc_MB_info(void)
*	功能	   : 	开辟主板结构体空间
*	返回值:	主板信息结构体指针
*
*********************************************************************************************************/

pMB_info_t malloc_MB_info(void)
{
  int i=0;	
  pMB_info_t MB_info;
  
  MB_info = (pMB_info_t)malloc(sizeof(MB_info_t));
  memset(MB_info,0,sizeof(MB_info_t));
#ifdef MB_DEBUG  
  MB_malloc_size += sizeof(MB_info_t);
#endif  
#ifdef  __FUCTION_GLOBAL_CONFIG__
//全局配置	
	MB_info->MB_global_config = (pMB_global_config_t)malloc(sizeof(MB_global_config_t));
	memset(MB_info->MB_global_config,0,sizeof(MB_global_config_t));
	
#ifdef MB_DEBUG  	
	MB_malloc_size += sizeof(MB_global_config_t);
#endif 

	for(i=0;i<NET_NUMBER;i++)
		{
	MB_info->MB_global_config->global_config_net[i] = (pMB_global_config_net_t)malloc(sizeof(MB_global_config_net_t));
		memset(MB_info->MB_global_config->global_config_net[i],0,sizeof(MB_global_config_net_t));
#ifdef MB_DEBUG  		
		MB_malloc_size += sizeof(MB_global_config_net_t);
#endif 

		}
	for(i=0;i<COM_NUMBER;i++)
		{
	MB_info->MB_global_config->global_config_com[i] = (pMB_global_config_com_t)malloc(sizeof(MB_global_config_com_t));
	memset(MB_info->MB_global_config->global_config_com[i],0,sizeof(MB_global_config_com_t));
#ifdef MB_DEBUG  	
		MB_malloc_size += sizeof(MB_global_config_com_t);
#endif 

		}
#endif

#ifdef 	__FUCTION_SCREEN_SWITCH__    
//视频切换配置	
	MB_info->screen_switch = (pMB_screen_switch_t)malloc(sizeof(MB_screen_switch_t));
		memset(MB_info->screen_switch,0,sizeof(MB_screen_switch_t));
#ifdef MB_DEBUG  		
		MB_malloc_size += sizeof(MB_screen_switch_t);
#endif 

	MB_info->screen_switch->screen_switch_status= (unsigned char *)malloc(SCREEN_SWITCH_OUTPORT);
		memset(MB_info->screen_switch->screen_switch_status,0,SCREEN_SWITCH_OUTPORT);
#ifdef MB_DEBUG  		
		MB_malloc_size += SCREEN_SWITCH_OUTPORT;
#endif 

#endif
#ifdef 	__FUCTION_BOARDCARD_ONLINE__        
//板卡在线配置
	MB_info->boardcard_online= (pMB_boardcard_online_t)malloc(sizeof(MB_boardcard_online_t));
		memset(MB_info->boardcard_online,0,sizeof(MB_boardcard_online_t));
#ifdef MB_DEBUG  		
		MB_malloc_size += sizeof(MB_boardcard_online_t);
#endif 

	MB_info->boardcard_online->boardcard_online_status= (unsigned char *)malloc(SCREEN_SWITCH_OUTPORT+SCREEN_SWITCH_INPORT);
		memset(MB_info->boardcard_online->boardcard_online_status,0,SCREEN_SWITCH_OUTPORT+SCREEN_SWITCH_INPORT);
#ifdef MB_DEBUG  

		MB_malloc_size += (SCREEN_SWITCH_OUTPORT+SCREEN_SWITCH_INPORT);
#endif 

#endif
#ifdef 	__FUCTION_BOARDCARD_INFO__        
//板卡ID
	MB_info->boardcard_info= (pMB_boardcard_info_t)malloc(sizeof(MB_boardcard_info_t));
		memset(MB_info->boardcard_info,0,sizeof(MB_boardcard_info_t));
#ifdef MB_DEBUG  		
		MB_malloc_size += sizeof(MB_boardcard_info_t);
#endif 

#endif 
#ifdef 	__FUCTION_SENCE_SWITCH__        
//场景配置
	MB_info->sence_switch= (pMB_sence_switch_t)malloc(sizeof(MB_sence_switch_t));
		memset(MB_info->sence_switch,0,sizeof(MB_sence_switch_t));
#ifdef MB_DEBUG  		
		MB_malloc_size += sizeof(MB_sence_switch_t);
#endif 
	for(i=0;i<SENCE_SWITCH_NUMBER;i++)
		{
		
		MB_info->sence_switch->sence_ID[i]= (unsigned char *)malloc(SCREEN_SWITCH_OUTPORT);
		memset(MB_info->sence_switch->sence_ID[i],0,SCREEN_SWITCH_OUTPORT);
		
#ifdef MB_DEBUG  

		MB_malloc_size += (SCREEN_SWITCH_OUTPORT);
#endif 
}

#endif
 
//--------如有新的功能--在此添加新的功能项---注意宏定义---//



//--------如有新的功能--在此添加新的功能项---注意宏定义---//


#ifdef MB_DEBUG  
		APP_TRACE("主板配置 MALLOC 空间的大小为: %d ",MB_malloc_size);
#endif 
		return MB_info;

}

/********************************************************************************************************
*	函数名:  	int MB_CONFIG_INIT(void)
*	功能	   : 	初始化主板信息结构体
*	返回值:	成功
*
*********************************************************************************************************/

int MB_CONFIG_INIT(void)
{
#ifdef MB_DEBUG  
 	int total_malloc_size = 0;
#endif	

	pCurrent_MB_info = malloc_MB_info();

#ifdef MB_DEBUG  
	APP_TRACE("主板配置 MALLOC 总空间的大小为: %d ",total_malloc_size);
#endif 
	return 1;
}


































#pragma pack ()

#ifdef __cplusplus
}
#endif


