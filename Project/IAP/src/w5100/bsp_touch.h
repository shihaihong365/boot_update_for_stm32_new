
#include  <stm32f10x_conf.h>
//#include  <includes.h>

#ifndef __BSP_TOUCH_H__
#define __BSP_TOUCH_H__

#define TOUCH_MSR_Y  0xD0   //��X������ָ�� addr:1
#define TOUCH_MSR_X  0x90   //��Y������ָ�� addr:3

#define TP_NCS_PIN				GPIO_Pin_6
#define TP_NCS_GPIO_PORT		GPIOG
#define TP_NCS_GPIO_CLK			RCC_APB2Periph_GPIOG

#define TP_IIC_DATA		GPIO_Pin_7
#define TP_IIC_SCL		GPIO_Pin_6
#define TP_IIC_GPIO_PORT	GPIOB


#define TP_INT_PIN				GPIO_Pin_8
#define TP_INT_GPIO_PORT		GPIOB
#define TP_INT_GPIO_CLK			RCC_APB2Periph_GPIOB
#define	TP_INT_PORT_SOURCE		GPIO_PortSourceGPIOB
#define	TP_INT_PIN_SOURCE		GPIO_PinSource8
#define	TP_INT_ID				BSP_INT_ID_EXTI15_10
#define	TP_INT_LINE				EXTI_Line8
#define	TP_INT_IRQn				EXTI15_10_IRQn


#define	TP_SPI					SPI2
#define	TP_SPI_CLK				RCC_APB1Periph_SPI2
#define	TP_SPI_PIN				GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15
#define	TP_SPI_GPIO_PORT		GPIOB
#define TP_SPI_GPIO_CLK			RCC_APB2Periph_GPIOB
//#define	TP_SPI_REMAP			GPIO_Remap_SPI2

#define	BSP_TOUCH_cs_set()		GPIO_SetBits(TP_NCS_GPIO_PORT, TP_NCS_PIN)
#define	BSP_TOUCH_cs_clr()		GPIO_ResetBits(TP_NCS_GPIO_PORT, TP_NCS_PIN)
#define	BSP_TOUCH_IntStatus()	GPIO_ReadInputDataBit(TP_INT_GPIO_PORT, TP_INT_PIN)
#define	BSP_TOUCH_INT_EN()		do {	\
								EXTI_ClearITPendingBit(TP_INT_LINE);	\
								BSP_IntEn(TP_INT_ID);	\
								}while(0)
#define	BSP_TOUCH_INT_DIS()		do {	\
								EXTI_ClearITPendingBit(TP_INT_LINE);	\
								BSP_IntDis(TP_INT_ID);	\
								}while(0)

void BSP_TOUCH_Init(void);
void BSP_TOUCH_IntEnable(unsigned char state);
unsigned char BSP_TOUCH_IO(unsigned char data);
void BSP_TOUCH_Read(unsigned short *x, unsigned short *y);

//extern OS_SEM BSP_TOUCH_ISR_Sem;
void BSP_TP_ISR(void);

#endif

