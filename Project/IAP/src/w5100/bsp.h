/*
*********************************************************************************************************
*                                     MICIRUM BOARD SUPPORT PACKAGE
*
*                             (c) Copyright 2007; Micrium, Inc.; Weston, FL
*
*               All rights reserved.  Protected by international copyright laws.
*               Knowledge of the source code may NOT be used to develop a similar product.
*               Please help us continue to provide the Embedded community with the finest
*               software available.  Your honesty is greatly appreciated.
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*
*                                        BOARD SUPPORT PACKAGE
*
*                                     ST Microelectronics STM32
*                                              on the
*
*                                     Micrium uC-Eval-STM32F107
*                                        Evaluation Board
*
* Filename      : bsp.h
* Version       : V1.00
* Programmer(s) : EHS
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                                 MODULE
*
* Note(s) : (1) This header file is protected from multiple pre-processor inclusion through use of the
*               BSP present pre-processor macro definition.
*********************************************************************************************************
*/

#ifndef  BSP_PRESENT
#define  BSP_PRESENT


/*
*********************************************************************************************************
*                                                 EXTERNS
*********************************************************************************************************
*/

#ifdef   BSP_MODULE
#define  BSP_EXT
#else
#define  BSP_EXT  extern
#endif


/*
*********************************************************************************************************
*                                              INCLUDE FILES
*********************************************************************************************************
*/

#include  <stdarg.h>
#include  <stdio.h>


/*
*********************************************************************************************************
*                                          GPIO PIN DEFINITIONS
*********************************************************************************************************
*/

/*
*********************************************************************************************************
*                                               INT DEFINES
*********************************************************************************************************
*/

enum
{
	BSP_INT_ID_WWDG = 0,		// Window WatchDog Interrupt
	BSP_INT_ID_PVD,				// PVD through EXTI Line detection Interrupt
	BSP_INT_ID_TAMPER,			// Tamper Interrupt
	BSP_INT_ID_RTC,				// RTC global Interrupt
	BSP_INT_ID_FLASH,			// FLASH global Interrupt
	BSP_INT_ID_RCC,				// RCC global Interrupt
	BSP_INT_ID_EXTI0,			// EXTI Line0 Interrupt
	BSP_INT_ID_EXTI1,			// EXTI Line1 Interrupt
	BSP_INT_ID_EXTI2,			// EXTI Line2 Interrupt
	BSP_INT_ID_EXTI3,			// EXTI Line3 Interrupt
	BSP_INT_ID_EXTI4,			// EXTI Line4 Interrupt
	BSP_INT_ID_DMA1_CH1,		// DMA1 Channel 1 global Interrupt
	BSP_INT_ID_DMA1_CH2,		// DMA1 Channel 2 global Interrupt
	BSP_INT_ID_DMA1_CH3,		// DMA1 Channel 3 global Interrupt
	BSP_INT_ID_DMA1_CH4,		// DMA1 Channel 4 global Interrupt
	BSP_INT_ID_DMA1_CH5,		// DMA1 Channel 5 global Interrupt
	BSP_INT_ID_DMA1_CH6,		// DMA1 Channel 6 global Interrupt
	BSP_INT_ID_DMA1_CH7,		// DMA1 Channel 7 global Interrupt
	BSP_INT_ID_ADC1_2,			// ADC1 et ADC2 global Interrupt
	BSP_INT_ID_CAN1_TX,			// CAN1 TX Interrupts
	BSP_INT_ID_CAN1_RX0,		// CAN1 RX0 Interrupts
	BSP_INT_ID_CAN1_RX1,		// CAN1 RX1 Interrupt
	BSP_INT_ID_CAN1_SCE,		// CAN1 SCE Interrupt
	BSP_INT_ID_EXTI9_5,			// External Line[9:5] Interrupts
	BSP_INT_ID_TIM1_BRK,		// TIM1 Break Interrupt
	BSP_INT_ID_TIM1_UP,			// TIM1 Update Interrupt
	BSP_INT_ID_TIM1_TRG_COM,	// TIM1 Trigger and Commutation Interrupt
	BSP_INT_ID_TIM1_CC,			// TIM1 Capture Compare Interrupt
	BSP_INT_ID_TIM2,			// TIM2 global Interrupt
	BSP_INT_ID_TIM3,			// TIM3 global Interrupt
	BSP_INT_ID_TIM4,			// TIM4 global Interrupt
	BSP_INT_ID_I2C1_EV,			// I2C1 Event Interrupt
	BSP_INT_ID_I2C1_ER,			// I2C1 Error Interrupt
	BSP_INT_ID_I2C2_EV,			// I2C2 Event Interrupt
	BSP_INT_ID_I2C2_ER,			// I2C2 Error Interrupt
	BSP_INT_ID_SPI1,			// SPI1 global Interrupt
	BSP_INT_ID_SPI2,			// SPI2 global Interrupt
	BSP_INT_ID_USART1,			// USART1 global Interrupt
	BSP_INT_ID_USART2,			// USART2 global Interrupt
	BSP_INT_ID_USART3,			// USART3 global Interrupt
	BSP_INT_ID_EXTI15_10,		// External Line[15:10] Interrupts
	BSP_INT_ID_RTC_ALARM,		// RTC Alarm through EXTI Line Interrupt
	BSP_INT_ID_OTG_FS_WKUP,		// USB WakeUp from suspend through EXTI Line Interrupt

	BSP_INT_ID_TIM8_BRK,		// TIM8 Break Interrupt
	BSP_INT_ID_TIM8_UP,			// TIM8 Update Interrupt
	BSP_INT_ID_TIM8_TRG_COM,	// TIM8 Trigger and Commutation Interrupt
	BSP_INT_ID_TIM8_CC,			// TIM8 Capture Compare Interrupt
	BSP_INT_ID_ADC3,			// ADC3 global Interrupt
	BSP_INT_ID_FSMC,			// FSMC global Interrupt
	BSP_INT_ID_SDIO,			// SDIO global Interrupt

	BSP_INT_ID_TIM5,			// TIM5 global Interrupt
	BSP_INT_ID_SPI3,			// SPI3 global Interrupt
	BSP_INT_ID_USART4,			// USART4 global Interrupt
	BSP_INT_ID_USART5,			// USRT5 global Interrupt
	BSP_INT_ID_TIM6,			// TIM6 global Interrupt
	BSP_INT_ID_TIM7,			// TIM7 global Interrupt
	BSP_INT_ID_DMA2_CH1,		// DMA2 Channel 1 global Interrupt
	BSP_INT_ID_DMA2_CH2,		// DMA2 Channel 2 global Interrupt
	BSP_INT_ID_DMA2_CH3,		// DMA2 Channel 3 global Interrupt
	BSP_INT_ID_DMA2_CH4,		// DMA2 Channel 4 global Interrupt
	BSP_INT_ID_DMA2_CH4_5,		// DMA2 Channel 4 & 5 global Interrupt
};
/*
*********************************************************************************************************
*                                             PERIPH DEFINES
*********************************************************************************************************
*/

enum
{
	BSP_PERIPH_ID_DMA1		        =  0 + 0,	//((uint32_t)0x00000001)
	BSP_PERIPH_ID_DMA2		        =  0 + 1,	//((uint32_t)0x00000002)
	BSP_PERIPH_ID_SRAM		        =  0 + 2,	//((uint32_t)0x00000004)
	BSP_PERIPH_ID_FLITF		        =  0 + 4,	//((uint32_t)0x00000010)
	BSP_PERIPH_ID_CRC		        =  0 + 6,	//((uint32_t)0x00000040)
	BSP_PERIPH_ID_FSMC		        =  0 + 8,	//((uint32_t)0x00000100)
	BSP_PERIPH_ID_SDIO              =  0 + 10,	//((uint32_t)0x00000400)
	BSP_PERIPH_ID_OTG_FS            =  0 + 12,	//((uint32_t)0x00001000)
	BSP_PERIPH_ID_ETH_MAC           =  0 + 14,	//((uint32_t)0x00004000)
	BSP_PERIPH_ID_ETH_MAC_TX        =  0 + 15,	//((uint32_t)0x00008000)
	BSP_PERIPH_ID_ETH_MAC_RX        =  0 + 16,	//((uint32_t)0x00010000)

	BSP_PERIPH_ID_AFIO              = 32 + 0,	//((uint32_t)0x00000001)
	BSP_PERIPH_ID_GPIOA             = 32 + 2,	//((uint32_t)0x00000004)
	BSP_PERIPH_ID_GPIOB             = 32 + 3,	//((uint32_t)0x00000008)
	BSP_PERIPH_ID_GPIOC             = 32 + 4,	//((uint32_t)0x00000010)
	BSP_PERIPH_ID_GPIOD             = 32 + 5,	//((uint32_t)0x00000020)
	BSP_PERIPH_ID_GPIOE             = 32 + 6,	//((uint32_t)0x00000040)
	BSP_PERIPH_ID_GPIOF             = 32 + 7,	//((uint32_t)0x00000080)
	BSP_PERIPH_ID_GPIOG             = 32 + 8,	//((uint32_t)0x00000100)
	BSP_PERIPH_ID_ADC1              = 32 + 9,	//((uint32_t)0x00000200)
	BSP_PERIPH_ID_ADC2              = 32 + 10,	//((uint32_t)0x00000400)
	BSP_PERIPH_ID_TIM1              = 32 + 11,	//((uint32_t)0x00000800)
	BSP_PERIPH_ID_SPI1              = 32 + 12,	//((uint32_t)0x00001000)
	BSP_PERIPH_ID_TIM8              = 32 + 13,	//((uint32_t)0x00002000)
	BSP_PERIPH_ID_USART1            = 32 + 14,	//((uint32_t)0x00004000)
	BSP_PERIPH_ID_ADC3              = 32 + 15,	//((uint32_t)0x00008000)
	BSP_PERIPH_ID_TIM15             = 32 + 16,	//((uint32_t)0x00010000)
	BSP_PERIPH_ID_TIM16             = 32 + 17,	//((uint32_t)0x00020000)
	BSP_PERIPH_ID_TIM17             = 32 + 18,	//((uint32_t)0x00040000)
	BSP_PERIPH_ID_TIM9              = 32 + 19,	//((uint32_t)0x00080000)
	BSP_PERIPH_ID_TIM10             = 32 + 20,	//((uint32_t)0x00100000)
	BSP_PERIPH_ID_TIM11             = 32 + 21,	//((uint32_t)0x00200000)

	BSP_PERIPH_ID_TIM2              = 64 + 0,	//((uint32_t)0x00000001)
	BSP_PERIPH_ID_TIM3              = 64 + 1,	//((uint32_t)0x00000002)
	BSP_PERIPH_ID_TIM4              = 64 + 2,	//((uint32_t)0x00000004)
	BSP_PERIPH_ID_TIM5              = 64 + 3,	//((uint32_t)0x00000008)
	BSP_PERIPH_ID_TIM6              = 64 + 4,	//((uint32_t)0x00000010)
	BSP_PERIPH_ID_TIM7              = 64 + 5,	//((uint32_t)0x00000020)
	BSP_PERIPH_ID_TIM12             = 64 + 6,	//((uint32_t)0x00000040)
	BSP_PERIPH_ID_TIM13             = 64 + 7,	//((uint32_t)0x00000080)
	BSP_PERIPH_ID_TIM14             = 64 + 8,	//((uint32_t)0x00000100)
	BSP_PERIPH_ID_WWDG              = 64 + 11,	//((uint32_t)0x00000800)
	BSP_PERIPH_ID_SPI2              = 64 + 14,	//((uint32_t)0x00004000)
	BSP_PERIPH_ID_SPI3              = 64 + 15,	//((uint32_t)0x00008000)
	BSP_PERIPH_ID_USART2            = 64 + 17,	//((uint32_t)0x00020000)
	BSP_PERIPH_ID_USART3            = 64 + 18,	//((uint32_t)0x00040000)
	BSP_PERIPH_ID_UART4             = 64 + 19,	//((uint32_t)0x00080000)
	BSP_PERIPH_ID_UART5             = 64 + 20,	//((uint32_t)0x00100000)
	BSP_PERIPH_ID_I2C1              = 64 + 21,	//((uint32_t)0x00200000)
	BSP_PERIPH_ID_I2C2              = 64 + 22,	//((uint32_t)0x00400000)
	BSP_PERIPH_ID_USB               = 64 + 23,	//((uint32_t)0x00800000)
	BSP_PERIPH_ID_CAN1              = 64 + 25,	//((uint32_t)0x02000000)
	BSP_PERIPH_ID_CAN2              = 64 + 26,	//((uint32_t)0x04000000)
	BSP_PERIPH_ID_BKP               = 64 + 27,	//((uint32_t)0x08000000)
	BSP_PERIPH_ID_PWR               = 64 + 28,	//((uint32_t)0x10000000)
	BSP_PERIPH_ID_DAC               = 64 + 29,	//((uint32_t)0x20000000)
	BSP_PERIPH_ID_CEC               = 64 + 30,	//((uint32_t)0x40000000)
};



void         App_NMI_ISR                 (void);
void         App_Fault_ISR               (void);
void         App_BusFault_ISR            (void);
void         App_UsageFault_ISR          (void);
void         App_MemFault_ISR            (void);
void         App_Spurious_ISR            (void);
//void         App_Reserved_ISR            (void);

void         BSP_IntHandlerWWDG          (void);
void         BSP_IntHandlerPVD           (void);
void         BSP_IntHandlerTAMPER        (void);
void         BSP_IntHandlerRTC           (void);
void         BSP_IntHandlerFLASH         (void);
void         BSP_IntHandlerRCC           (void);
void         BSP_IntHandlerEXTI0         (void);
void         BSP_IntHandlerEXTI1         (void);
void         BSP_IntHandlerEXTI2         (void);
void         BSP_IntHandlerEXTI3         (void);
void         BSP_IntHandlerEXTI4         (void);
void         BSP_IntHandlerDMA1_CH1      (void);
void         BSP_IntHandlerDMA1_CH2      (void);
void         BSP_IntHandlerDMA1_CH3      (void);
void         BSP_IntHandlerDMA1_CH4      (void);
void         BSP_IntHandlerDMA1_CH5      (void);

void         BSP_IntHandlerDMA1_CH6      (void);
void         BSP_IntHandlerDMA1_CH7      (void);
void         BSP_IntHandlerADC1_2        (void);
void         BSP_IntHandlerCAN1_TX       (void);
void         BSP_IntHandlerCAN1_RX0      (void);
void         BSP_IntHandlerCAN1_RX1      (void);
void         BSP_IntHandlerCAN1_SCE      (void);
void         BSP_IntHandlerEXTI9_5       (void);
void         BSP_IntHandlerTIM1_BRK      (void);
void         BSP_IntHandlerTIM1_UP       (void);
void         BSP_IntHandlerTIM1_TRG_COM  (void);
void         BSP_IntHandlerTIM1_CC       (void);
void         BSP_IntHandlerTIM2          (void);
void         BSP_IntHandlerTIM3          (void);
void         BSP_IntHandlerTIM4          (void);
void         BSP_IntHandlerI2C1_EV       (void);

void         BSP_IntHandlerI2C1_ER       (void);
void         BSP_IntHandlerI2C2_EV       (void);
void         BSP_IntHandlerI2C2_ER       (void);
void         BSP_IntHandlerSPI1          (void);
void         BSP_IntHandlerSPI2          (void);
void         BSP_IntHandlerUSART1        (void);
void         BSP_IntHandlerUSART2        (void);
void         BSP_IntHandlerUSART3        (void);
void         BSP_IntHandlerEXTI15_10     (void);
void         BSP_IntHandlerRTCAlarm      (void);
void         BSP_IntHandlerUSBWakeUp     (void);

void         BSP_IntHandlerTIM8_BRK      (void);
void         BSP_IntHandlerTIM8_UP       (void);
void         BSP_IntHandlerTIM8_TRG_COM  (void);
void         BSP_IntHandlerTIM8_CC       (void);
void         BSP_IntHandlerADC3          (void);
void         BSP_IntHandlerFSMC          (void);
void         BSP_IntHandlerSDIO          (void);

void         BSP_IntHandlerTIM5          (void);
void         BSP_IntHandlerSPI3          (void);
void         BSP_IntHandlerUSART4        (void);
void         BSP_IntHandlerUSART5        (void);
void         BSP_IntHandlerTIM6          (void);
void         BSP_IntHandlerTIM7          (void);
void         BSP_IntHandlerDMA2_CH1      (void);
void         BSP_IntHandlerDMA2_CH2      (void);
void         BSP_IntHandlerDMA2_CH3      (void);
void         BSP_IntHandlerDMA2_CH4_5    (void);



#endif                                                          /* End of module include.                               */
