#ifdef __cplusplus
extern "C" {
#endif
#include "unmap_table.h"
#include "unmap_table.h"
#pragma pack (1)

pUNMAP_TABLE_HEAD global_unmap_t;
/*
void BSP_UNMAP_Init()
{
global_unmap_t = create_unmap_table(2,50,256,50,256);
}
*/

#ifdef __MULTIPLE_ACCESS__ 
pUNMAP_TABLE_HEAD create_unmap_table(int user_number_total, ...)
{
    pUNMAP_TABLE_HEAD phead;
    int i=0,j=0,k=0;
    int umap_total_max_index = 0;
    va_list argp;		                            //定义保存函数参数的结构 */    
    int argno = 0;                                          //纪录参数个数 */ 
    unsigned int* para = NULL;                                       //参数数组 */  
    
//-------------------------处理参数个数---存入para待用-------------------------------------------//   
    para = (unsigned int*)malloc((user_number_total*2+1)*sizeof(user_number_total)); 
    memset(para,0,(user_number_total*2+1)*sizeof(user_number_total));
    //取出参数值并存入数组 待用
    va_start(argp, user_number_total);                             // argp指向传入的第一个可选参数 UserNumber是最后一个确定的参数 
    para[0] = user_number_total;
    //取出除第一个所有剩下的的参数  
    while(1)
    {
      if ( argno<(para[0]*2))
      {
       argno++;  
       para[argno] = va_arg( argp, int);                    //取出当前的参数，类型为int.              
      }
      else
      {
        va_end( argp );                                     //将argp置为NULL   
       //printf("不定参数个数共有 %d 个 \r\n",argno);
       //printf("总参数个数共有 %d 个 \r\n",argno+1);
        break;
      }
    }
//-------------------------------------------para[]数组结构------------------------------------------------------//
    
//------para[0]------------para[1]--------------para[2]------------ para[3]-------------- para[4]--------依此类推//
//----最大用户数----用户1的最大块数-----用户1的块的buf的大小----用户2的最大块数----用户2的块的buf的大小--依此类推// 
    
//-------------------------处理参数个数---存入para待用-----------------------------------------------------------//
    
//-------------------------初始化---unmap------------------------------------------------------------------------// 
    
    //创建并初始化umap结构体指针   
    phead=(pUNMAP_TABLE_HEAD)malloc(sizeof(UNMAP_TABLE_HEAD));//申请空间，free记得释放
    memset((unsigned char*)phead, 0, sizeof(UNMAP_TABLE_HEAD));
    
    phead->user_max_number= para[0];                    //用来存放用户数;
    phead->alluser_get_num= 0;                          //初始取为0;
    
    //用来存放索引所有用户的标记位，即MAX_INDEX之和;
    for(i=0;i<phead->user_max_number;i++)
    {
      umap_total_max_index += para[i*2+1]; 
    }

    phead->alluser_max_index = umap_total_max_index;    //用来写入总的块数
    
    phead->alluser_index_flag=(unsigned char*)malloc(phead->alluser_max_index);//创建用来存放索引所有用户的标记位的char数组，//申请空间，free记得释放
    memset(phead->alluser_index_flag,0,phead->alluser_max_index );
   //记录每个用户的写标记范围 
    phead->singleuser_set_num = (unsigned int*)malloc(phead->user_max_number*sizeof(phead->user_max_number));//初始取为0;//申请空间，free记得释放
    memset(phead->singleuser_set_num,0,phead->user_max_number*sizeof(phead->user_max_number));
    //记录每个用户所需的最大块数
    phead->singleuser_max_index = (unsigned int*)malloc(phead->user_max_number*sizeof(phead->user_max_number));//初始取为0;//申请空间，free记得释放
    memset(phead->singleuser_max_index,0,phead->user_max_number*sizeof(phead->user_max_number));
    //记录每个用户所需的块数buf大小 
    phead->singleuser_index_size = (unsigned int*)malloc(phead->user_max_number*sizeof(phead->user_max_number));//初始取为0;//申请空间，free记得释放
    memset(phead->singleuser_index_size,0,phead->user_max_number*sizeof(phead->user_max_number));
    for(i=0;i<phead->user_max_number;i++) 
    {
      phead->singleuser_max_index[i] =  para[i*2+1];
      phead->singleuser_index_size[i] =  para[(i+1)*2];
    }
    //创建用来存放索引所有用户用的块的BUF指针;单个用户分别占用某一部分
    phead->alluser_index_list=(unsigned char**)malloc(phead->alluser_max_index * sizeof(phead->alluser_index_list));//umap_total_max_index 乘以 指针长度
    for(i = 0;i<phead->user_max_number;i++)
    {
      for (j=k;j<k+phead->singleuser_max_index[i]; j++)
      {
          phead->alluser_index_list[j]=(unsigned char*)malloc(phead->singleuser_index_size[i]);//申请空间，free记得释放
          memset(phead->alluser_index_list[j],0,phead->singleuser_index_size[i]);
      }
      k = phead->singleuser_max_index[i];
    }

    //每个空间已使用使用的有效长度标志;
    phead->alluser_used_size=(unsigned int*)malloc(phead->alluser_max_index*sizeof(phead->alluser_max_index));
    memset(phead->alluser_used_size,0,phead->alluser_max_index*sizeof(phead->alluser_max_index));
    free(para);
    return phead;
}
#endif
#ifdef __SINGLE_ACCESS__
//新建一个unmap,    max_index:多少个块       size_of_index:块长度
pUNMAP_TABLE_HEAD create_unmap_table(int max_index, int size_of_index)
{
    pUNMAP_TABLE_HEAD phead;
    int i=0;
    
    phead=(pUNMAP_TABLE_HEAD)malloc(sizeof(UNMAP_TABLE_HEAD));
    memset((unsigned char*)phead, 0, sizeof(UNMAP_TABLE_HEAD));
    
    //用来存放索引的标记位;
    phead->index_flag=(unsigned char*)malloc(max_index);
    memset(phead->index_flag, 0, max_index);
    
    //用来存放BUF指针;
    phead->index_list=(unsigned char**)malloc(max_index*4);
    
    for (i=0; i<max_index; i++)
    {
        phead->index_list[i]=(unsigned char*)malloc(size_of_index);
    }
    //每个空间使用的有效长度;
    phead->used_size=(unsigned int*)malloc(max_index*4);
        
    phead->max_index=max_index;
    phead->get_num=0;
    phead->set_num=0;
    phead->index_size=size_of_index;
    
    return phead;
}
#endif
#ifdef __MULTIPLE_ACCESS__
//从unmap取出一个块，      buf:取出的块放什么地方         bufsize:外部传入的buf长度
int get_from_table(pUNMAP_TABLE_HEAD phead, unsigned char* buf, int bufsize)
{
    int i=0,j=0,k=0;
    int len=0;
      
    //len=(phead->index_size<bufsize?phead->index_size:bufsize);
    
    for (i=0;i<phead->alluser_max_index;i++,phead->alluser_get_num++)//将所有的块遍历一遍，alluser_get_num 循环加1
    {
        if (phead->alluser_get_num > phead->alluser_max_index)
        {
            phead->alluser_get_num=0;
        }
 
        if(phead->alluser_index_flag[phead->alluser_get_num] != 0)//标志不为0代表有数据
        {
          for(j=0;j<phead->user_max_number;j++)//循环所有用户数
          {
                if(phead->alluser_get_num<k+phead->singleuser_max_index[j])//判断属于哪一个用户
                {
                  len=(phead->singleuser_index_size[j] < phead->alluser_used_size[phead->alluser_get_num] ? phead->singleuser_index_size[j] : phead->alluser_used_size[phead->alluser_get_num]);//取有效数据长度 取最小值
                  len=(len<bufsize?len:bufsize); //再与传入的buf长度作比较 取最小值
                  memcpy(buf, phead->alluser_index_list[phead->alluser_get_num], len);
                  phead->alluser_index_flag[phead->alluser_get_num]=0;//标志位置0，可重新使用
                  phead->alluser_used_size[phead->alluser_get_num]=0;//有效数据长度位置0 当前无数据
                  return len;                  
                }
                k = phead->singleuser_max_index[j];//第二个用户从从第一个用户的尾部开始 依此类推
          }
        }
    }
return 0;
}
#endif
#ifdef __SINGLE_ACCESS__
//从unmap取出一个块，      buf:取出的块放什么地方         bufsize:外部传入的buf长度
int get_from_table(pUNMAP_TABLE_HEAD phead, unsigned char* buf, int bufsize)
{
    int i=0;
    int len=0;
    
    //len=(phead->index_size<bufsize?phead->index_size:bufsize);
    
    for (i=0;i<phead->max_index;i++,phead->get_num++)
    {
        if (phead->get_num >= phead->max_index)
        {
            phead->get_num=0;
        }
        
        if(phead->index_flag[i] != 0)
        {
            len=(phead->index_size < phead->used_size[i] ? phead->index_size : phead->used_size[i]);
            memcpy(buf, phead->index_list[i], len);
            phead->index_flag[i]=0;
            phead->used_size[i]=0;//有效空间清除;
            return len;
        }
    }

return 0;
}
#endif

#ifdef __MULTIPLE_ACCESS__
//向unmap写入一个块，       buf:块指针         bufsize:外部传入的buf长度
int set_to_table(pUNMAP_TABLE_HEAD phead,int user_number,unsigned char* buf, int bufsize)
{
    int i=0,j=0,index_flag_offset=0;
    int len=0;    
    if(user_number < phead->user_max_number)//如果小于总用户数则进入
    {
     //判断有效长度
      len=(phead->singleuser_index_size[user_number]<bufsize?phead->singleuser_index_size[user_number]:bufsize); 
	}
    else
    {
      return 0;    
    }
    

      for (i=0;i<phead->singleuser_max_index[user_number];i++,phead->singleuser_set_num[user_number]++)
      {
          if (phead->singleuser_set_num[user_number]>phead->singleuser_max_index[user_number])
          {
            phead->singleuser_set_num[user_number]=0;
          }
		 //计算偏移量改成重入可用的
          for(j=1;j<user_number;j++)//从第一位用户开始算
          {
          index_flag_offset += phead->singleuser_max_index[j-1];//前一位用户
          }
          index_flag_offset += phead->singleuser_set_num[user_number];//加上偏移量 
 
          
          if(phead->alluser_index_flag[index_flag_offset]==0)
          {          	  
              memcpy(phead->alluser_index_list[index_flag_offset],buf,len);
              phead->alluser_index_flag[index_flag_offset]=1;
              phead->alluser_used_size[index_flag_offset]=len;
              return len;
          } 
          else
          {
            index_flag_offset = 0;
          }
      } 

    
    return 0;
}
#endif

#ifdef __SINGLE_ACCESS__
//向unmap写入一个块，       buf:块指针         bufsize:外部传入的buf长度
int set_to_table(pUNMAP_TABLE_HEAD phead, unsigned char* buf, int bufsize)
{
    int i=0;
    int len=0;
     
    len=(phead->index_size<bufsize?phead->index_size:bufsize);
    
    //for (i=phead->set_num;i<phead->max_index;i++)
    for (i=0;i<phead->max_index;i++,phead->set_num++)
    {
        if (phead->set_num>=phead->max_index)
        {
            phead->set_num=0;
        }
        
        if(phead->index_flag[i]==0)
        {
            memcpy(phead->index_list[i],buf,len);
            phead->index_flag[i]=1;
            phead->used_size[i]=len;
            return len;
        }
    }  
    
    return 0;
}
#endif

#ifdef __MULTIPLE_ACCESS__ 
//删除一个unmap,
void delete_unmap_table(pUNMAP_TABLE_HEAD phead)
{
    int i=0;
    //int res;
    
    for (i=0;i<phead->alluser_max_index;i++)
    {
        free(phead->alluser_index_list[i]);
    }
           
    //用来存放索引的标记位;
    
    free(phead->alluser_index_flag);
    free(phead->alluser_used_size);
    free(phead->singleuser_set_num);
    free(phead->singleuser_max_index);
    free(phead->singleuser_index_size);
    free(phead);
}
#endif

#ifdef __SINGLE_ACCESS__
//删除一个unmap,
void delete_unmap_table(pUNMAP_TABLE_HEAD phead)
{
    int i=0;
    //int res;
    
    for (i=0;i<phead->max_index;i++)
    {
        free(phead->index_list[i]);
    }
           
    //用来存放索引的标记位;
    
    free(phead->index_flag);
    free(phead->index_list);
    free(phead->used_size);
    
    free(phead);
}
#endif

#pragma pack ()

#ifdef __cplusplus
}
#endif


