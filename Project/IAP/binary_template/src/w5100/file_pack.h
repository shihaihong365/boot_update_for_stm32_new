#ifndef __FILE_PACK_H__
#define __FILE_PACK_H__




#ifdef __cplusplus
extern "C" {
#endif


#define  V_GET_APP_FILE_INFO  0X00014
#define  V_GET_APP_FILE_BLOCK 0X00015

#define FILE_IS_NOT_GET 0
#define FILE_IS_GET 1
#define FILE_GET_COMPLETE -1


#pragma pack(1)

typedef struct fie_block
{	 
	unsigned int offset;
	unsigned int size;
	unsigned int total_pack_size;
	unsigned int name_len;
	char name;
}file_block_t;

typedef struct file_data
{	 
	unsigned int file_data_len;
	char data_buff;
}file_data_t;

typedef struct fie_desc_info
{	 
	unsigned int buffer_size;
	unsigned int file_size;
	unsigned int offset;
	unsigned int size;	
	unsigned int total_pack_size;

	unsigned int name_len;
	char name;

}fie_desc_info_t;

typedef struct udphdr 
{
	unsigned short   source;
	unsigned short	dest;
	unsigned short	len;
	unsigned short	check;
}udphdr_t;

#pragma pack()

#ifdef __cplusplus
}
#endif

#endif


