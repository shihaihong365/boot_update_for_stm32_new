
#include  <stm32f10x_conf.h>

#ifndef __BSP_FSMC_H__
#define __BSP_FSMC_H__

#define	FSMC_EXT_MEM					((__IO unsigned char*)0x64000000)
#define	FSMC_EXT_MEM_SIZE				0x80000

void BSP_FSMC_Init(void);

#endif

