
/***********************************************************************************************
*	 						 	-w5100-sock 0-UDP
*							2015年8月29日16:03:54			
*							autor		:	lxb
************************************************************************************************/

#ifndef __BSP_W5100_H__
#define __BSP_W5100_H__

#include  <stm32f10x_conf.h>

#include "unmap_table.h"

#define APP_TRACE u2_printf


//功能选择宏
#define _NET1_W5100_USE_
#define _NET2_W5100_USE_
//功能选择宏

#ifdef _NET1_W5100_USE_
#define W5100_RST_PIN			GPIO_Pin_3
#define W5100_RST_GPIO_PORT		GPIOD
#define W5100_RST_GPIO_CLK		RCC_APB2Periph_GPIOD

#define W5100_INT_PIN			GPIO_Pin_6
#define W5100_INT_GPIO_PORT		GPIOD
#define W5100_INT_GPIO_CLK		RCC_APB2Periph_GPIOD
#define	W5100_INT_PORT_SOURCE	GPIO_PortSourceGPIOD
#define	W5100_INT_PIN_SOURCE	GPIO_PinSource6
//#define	W5100_INT_ID			BSP_INT_ID_EXTI9_5
#define	W5100_INT_LINE			EXTI_Line6
#define	W5100_INT_IRQn			EXTI9_5_IRQn

#define	BSP_W5100_rst_set()		W5100_RST_GPIO_PORT->BSRR = W5100_RST_PIN
#define	BSP_W5100_rst_clr()		W5100_RST_GPIO_PORT->BRR = W5100_RST_PIN
#define	BSP_W5100_IntStatus()	GPIO_ReadInputDataBit(W5100_INT_GPIO_PORT, W5100_INT_PIN)
#endif

#ifdef _NET2_W5100_USE_
//[ysw Begin: 20140424 19:48:22]
#define W5100_RST_PIN2			GPIO_Pin_3
#define W5100_RST_GPIO_PORT2		GPIOD
#define W5100_RST_GPIO_CLK2		RCC_APB2Periph_GPIOD

#define W5100_INT_PIN2				GPIO_Pin_5
#define W5100_INT_GPIO_PORT2		GPIOE
#define W5100_INT_GPIO_CLK2		RCC_APB2Periph_GPIOE
#define	W5100_INT_PORT_SOURCE2	GPIO_PortSourceGPIOE
#define	W5100_INT_PIN_SOURCE2	GPIO_PinSource5
//#define	W5100_INT_ID2				BSP_INT_ID_EXTI9_5
#define	W5100_INT_LINE2			EXTI_Line5
#define	W5100_INT_IRQn2			EXTI9_5_IRQn

#define	BSP_W5100_rst_set2()		W5100_RST_GPIO_PORT2->BSRR = W5100_RST_PIN2
#define	BSP_W5100_rst_clr2()		W5100_RST_GPIO_PORT2->BRR = W5100_RST_PIN2
#define	BSP_W5100_IntStatus2()	GPIO_ReadInputDataBit(W5100_INT_GPIO_PORT2, W5100_INT_PIN2)
//[YSW End: 20140424 19:48:28]
#endif



/////////////////////////////////////////////////////////////////////////////////////////////
#ifdef _NET1_W5100_USE_
#define	W5100_BASE				0x60000000
#endif

#ifdef _NET2_W5100_USE_
#define	W5100_BASE2				0x6c000000
#endif

#define	W5100_COMM_REG_BASE		0x0000
#define	W5100_SOCK_REG_BASE		0x0400
#define	W5100_SOCK_REG_SIZE		0x0100
#define	W5100_TX_MEM_BASE		0x4000
#define	W5100_TX_MEM_SIZE		0x2000
#define	W5100_RX_MEM_BASE		0x6000
#define	W5100_RX_MEM_SIZE		0x2000

#define	W5100_MIN(x,y)			((x) < (y) ? (x) : (y))

enum
{
	W5100_SOCK_TX_MEM_SIZE	= 0x2000,
	W5100_SOCK_RX_MEM_SIZE	= 0x2000,
	
	W5100_PROTOCOL_CLOSED	= 0,
	W5100_PROTOCOL_TCP		= 1,
	W5100_PROTOCOL_UDP		= 2,
	W5100_PROTOCOL_IPRAW	= 3,
	W5100_PROTOCOL_MACRAW	= 4,
	W5100_PROTOCOL_PPPOE	= 5,
	
	W5100_COMMAND_OPEN		= 0x01,
	W5100_COMMAND_LISTEN	= 0x02,
	W5100_COMMAND_CONNECT	= 0x04,
	W5100_COMMAND_DISCON	= 0x08,
	W5100_COMMAND_CLOSE		= 0x10,
	W5100_COMMAND_SEND		= 0x20,
	W5100_COMMAND_SEND_MAC	= 0x21,
	W5100_COMMAND_SEND_KEEP	= 0x22,
	W5100_COMMAND_RECV		= 0x40,
	
	W5100_INT_MASK_CONFLICT	= 0x80,
	W5100_INT_MASK_UNREACH	= 0x40,
	W5100_INT_MASK_PPPOE	= 0x20,
	W5100_INT_MASK_S3_INT	= 0x08,
	W5100_INT_MASK_S2_INT	= 0x04,
	W5100_INT_MASK_S1_INT	= 0x02,
	W5100_INT_MASK_S0_INT	= 0x01,
	
	W5100_INT_FLAG_CONFLICT	= 0x80,
	W5100_INT_FLAG_UNREACH	= 0x40,
	W5100_INT_FLAG_PPPOE	= 0x20,
	W5100_INT_FLAG_S3_INT	= 0x08,
	W5100_INT_FLAG_S2_INT	= 0x04,
	W5100_INT_FLAG_S1_INT	= 0x02,
	W5100_INT_FLAG_S0_INT	= 0x01,
	
	W5100_INT_FLAG_SEND_OK	= 0x10,
	W5100_INT_FLAG_TIMEOUT	= 0x08,
	W5100_INT_FLAG_RECV		= 0x04,
	W5100_INT_FLAG_DISCON	= 0x02,
	W5100_INT_FLAG_CONNECT	= 0x01,
	
	W5100_STATUS_SOCK_CLOSED		= 0x00,	//当向Sn_CR 写入CLOSE 命令产生超时中断或连接被终止，将出现SOCK_CLOSED 状态。在SOCK_CLOSED 状态不产生任何操作，释放所有连接资源
	W5100_STATUS_SOCK_INIT			= 0x13,	//当Sn_MR 设为TCP 模式时，向Sn_CR 中写入OPEN 命令时将出现SOCK_INIT 状态。这是端口建立TCP 连接的开始。在SOCK_INIT 状态，Sn_CR 命令的类型决定了操作类型—TCP 服务器模式或TCP 客户端模式
	W5100_STATUS_SOCK_LISTEN		= 0x14,	//当端口处于SOCK_INIT 状态时，向Sn_CR 写入LISTEN 命令时将产生SOCK_LISTEN 状态。相应的端口设置为TCP 服务器模式，如果收到连接请求将改变为ESTABLISHED 状态
	W5100_STATUS_SOCK_ESTABLISHED	= 0x17,	//当端口建立连接时将产生SOCK_ESTABLISHED 状态，在这种状态下可以发送和接收TCP 数据
	W5100_STATUS_SOCK_CLOSE_WAIT	= 0x1C,	//当收到来自对端的终止连接请求时，将产生SOCK_CLOSE_WAIT 状态。在这种状态，从对端收到响应信息，但没有断开。当收到DICON 或CLOSE 命令时，连接断开
	W5100_STATUS_SOCK_UDP			= 0x22,	//当Sn_MR 设为UDP 模式，向Sn_CR 写入OPEN 命令时将产生SOCK_UDP 状态，在这种状态下通信不需要与对端建立连接，数据可以直接发送和接收
	W5100_STATUS_SOCK_IPRAW			= 0x32,	//当Sn_MR 设为IPRAW 模式，向Sn_CR 写入OPEN 命令时将产生SOCK_IPRAW 状态，在IP RAW 状态， IP 层以上的协议将不处理。详情请参考”IPRAW”
	W5100_STATUS_SOCK_MACRAW		= 0x42,	//当Sn_MR 设为MACRAW 模式，向S0_CR 写入OPEN 命令时将产生SOCK_MACRAW 状态。在MAC RAW 状态，所有协议数据包都不处理，详情请参考”MAC RAW”
	W5100_STATUS_SOCK_PPPOE			= 0x5F,	//当Sn_MR 设为PPPoE 模式，向S0_CR 写入OPEN 命令时将产生SOCK_PPPOE 状态
	
	W5100_STATUS_SOCK_SYNSENT		= 0x15,	//端口n 在SOCK_INIT 状态，向Sn_CR 写入CONNECT 命令时将出现SOCK_SYNSENT 状态。如果连接成功，将自动改变为SOCK_ESTABLISH
	W5100_STATUS_SOCK_SYNRECV		= 0x16,	//当接收到远程对端(客户端)发送的连接请求时，将出现SOCK_SYNRECV 状态。响应请求后，状态改变为SOCK_ESTABLISH
	W5100_STATUS_SOCK_FIN_WAIT		= 0x18,	//在连接终止过程中将出现这些状态。如果连接终止已完成，或产生了时间溢出中断，状态将自动被改变为SOCK_CLOSED
	W5100_STATUS_SOCK_CLOSING		= 0x1A,	//同上
	W5100_STATUS_SOCK_TIME_WAIT		= 0x1B,	//同上
	W5100_STATUS_SOCK_LAST_ACK		= 0x1D,	//同上
	W5100_STATUS_SOCK_ARP			= 0x11,	//在TCP 模式下发出连接请求或在UDP 模式下发送数据时，当发出ARP 请求以获得远程对端的物理地址时，将出现SOCK_ARP 状态。如果收到ARP 响应，将产生SOCK_SYNSENT 、SOCK_UDP 或SOCK_ICMP 状态，以便下面的操作
	//W5100_STATUS_SOCK_ARP			= 0x21,
	//W5100_STATUS_SOCK_ARP			= 0x31,
	
};

//#pragma pack(push)
//#pragma pack(1)

typedef struct w5100_comm_reg
{
	unsigned char	mode;
	unsigned char	gateway[4];
	unsigned char	subnet[4];
	unsigned char	src_mac[6];
	unsigned char	src_ip[4];
	unsigned char	rsv1[2];
	unsigned char	int_reg;
	unsigned char	int_mask;
	unsigned char	retry_time[2];
	unsigned char	retry_count;
	unsigned char	rx_mem_size;
	unsigned char	tx_mem_size;
	unsigned char	authentication_type[2];
	unsigned char	rsv2[10];
	unsigned char	ppp_lcp_request_timer;
	unsigned char	ppp_lcp_magic_number;
	unsigned char	unreachable_ip[4];
	unsigned char	unreachable_port[2];
	unsigned char	rsv3[976];
}w5100_comm_reg_t;

typedef struct w5100_sock_reg
{
	unsigned char	mode;
	unsigned char	command;
	unsigned char	int_reg;
	unsigned char	status;
	unsigned char	src_port[2];
	unsigned char	dst_mac[6];
	unsigned char	dst_ip[4];
	unsigned char	dst_port[2];
	unsigned char	max_segment_size[2];
	unsigned char	protocol_of_ipraw;
	unsigned char	ip_tos;
	unsigned char	ip_ttl;
	unsigned char	rsv1[9];
	unsigned char	tx_free_size[2];
	unsigned char	tx_read_pointer[2];
	unsigned char	tx_write_pointer[2];
	unsigned char	rx_received_size[2];
	unsigned char	rx_read_pointer[2];
	unsigned char	rsv2[2];
	unsigned char	rsv3[212];
}w5100_sock_reg_t;


//lxb 2015年8月28日21:40:55
typedef struct __W5100_UDP_LOCALHOST_INFO__
{
	unsigned char localhost_gateway[4];//本机网关
	unsigned char localhost_subnet[4];//本机子网掩码
	unsigned char localhost_IP[4];//本机IP	
	unsigned char localhost_mac[6];//本机mac地址	
	unsigned short localhost_UDPport;//本机udp端口号

}W5100_UDP_LOCALHOST_INFO,*pW5100_UDP_LOCALHOST_INFO;

typedef struct __W5100_UDP_SEND_HEAD__
{
	unsigned char dst_IP[4];//源地址
	unsigned char dst_Port[2];//源port
	unsigned char data_len[2];//不包含8字节包头，接收到的数据长度

}W5100_UDP_SEND_HEAD,*pW5100_UDP_SEND_HEAD;
//lxb 2015年8月28日21:40:55
typedef enum __CHIPSELECET__
{
#ifdef _NET1_W5100_USE_	
	NET_FIRST,
#endif
#ifdef _NET2_W5100_USE_		
	NET_SECEND,
#endif

	NET_INVALID
}CHIPSELECET;

//#pragma pack(pop)
#ifdef _NET1_W5100_USE_	
extern w5100_comm_reg_t* var_w5100_comm_reg;
extern w5100_sock_reg_t* var_w5100_sock_reg[4];
extern unsigned char* w5100_tx_mem;
extern unsigned char* w5100_rx_mem;
//lxb 2015年8月28日21:40:55
extern pUNMAP_TABLE_HEAD w5100net1_punmap;
//lxb 2015年8月28日21:40:55
#endif

#ifdef _NET2_W5100_USE_	
//[ysw Begin: 20140424 19:53:06]
extern w5100_comm_reg_t* var_w5100_comm_reg2;
extern w5100_sock_reg_t* var_w5100_sock_reg2[4];
extern unsigned char* w5100_tx_mem2;
extern unsigned char* w5100_rx_mem2;
//lxb 2015年8月28日21:40:55
extern pUNMAP_TABLE_HEAD w5100net2_punmap;
//lxb 2015年8月28日21:40:55
#endif

extern __IO unsigned char wcnt;

void BSP_W5100_UDP_Localhost_Config_set(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,CHIPSELECET chipselect);
void BSP_W5100_UDP_Localhost_Config_read(pW5100_UDP_LOCALHOST_INFO pudp_localhost_info,CHIPSELECET chipselect);
unsigned short BSP_W5100_UDP_SEND(pW5100_UDP_SEND_HEAD udp_send_head,CHIPSELECET chipselect, unsigned char* data, unsigned short len);

void BSP_W5100_Init(void);
void BSP_W5100_ISR(void);

#endif

