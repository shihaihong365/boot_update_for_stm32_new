#include "make_pack.h"

#ifdef _FUNCTION_PACKET_5555_
int make_pack_5555(unsigned char method, unsigned short cmd, unsigned short cmd_parameter_len, unsigned char* cmd_parameter_data_in, unsigned char * buf_out)
{
	static unsigned short msg_pack_count = 0;

	unsigned char crc_check_data = 0;

	unsigned short total_pack_len = 0;

	pack_5555_head_t head;

	head.syn = 0x5555;

	total_pack_len = (sizeof(pack_5555_head_t)+cmd_parameter_len+1);//包含CRC 1 char

	head.pack_len = EXCHANGE16BIT(total_pack_len);

	head.pack_count = EXCHANGE16BIT(msg_pack_count);

	msg_pack_count++;

	head.method = (method);

	head.cmd = EXCHANGE16BIT(cmd);
	head.cmd_parameter_len = EXCHANGE16BIT(cmd_parameter_len);
	
	memcpy(buf_out, &head, sizeof(pack_5555_head_t));//包头
	memcpy(buf_out+sizeof(pack_5555_head_t), cmd_parameter_data_in, cmd_parameter_len);//参数内容

	crc_check_data = 0;
	memcpy(buf_out+sizeof(pack_5555_head_t)+cmd_parameter_len,&crc_check_data,1);//crc长度1


	return total_pack_len;//加上CRC
}
#endif



