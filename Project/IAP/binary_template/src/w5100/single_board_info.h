

#ifndef __SINGLE_BOARD_INFO_
#define __SINGLE_BOARD_INFO_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <stdio.h>
#include "string.h"
#include <stdarg.h>
#include <stdlib.h>
#pragma pack (1)
//命令
/*
enum
{
	GET 			= 0x01,//请求获取内容
	SET 			= 0x02,//设置内容
	RETURN_GET		= 0x11,//返回请求内容
	RETURN_SET 		= 0x12,//返回设置结果
	EVENT			= 0x05,//从设备有变化主动通知主设备
};
*/




//功能选用宏定义
#define __FUNCTION_VIDEO_IN_INFO__	
#define __FUNCTION_VIDEO_OUT_INFO__	
#define __FUNCTION_AUDIO_IN_INFO__	
#define __FUNCTION_BASIC_INFO__	
#define __FUNCTION_CONTORL_INFO__	


//#define  SB_DEBUG  

//配置
enum
{
	SB_SYC_CONFIG_FLAG			= 0xEB9055AA,
	SB_HARDWARE_INPORT			= 18,
	SB_HARDWARE_OUTPORT 		        = 18,
};

//分辨率
enum
{
	resolution_720x480i60 = 0x01,resolution_720x576i50 = 0x02,resolution_720x480p60 = 0x03,
	resolution_720x576p50 = 0x04,resolution_1280x720p60 = 0x05,resolution_1280x720p59 = 0x06,
	resolution_1280x720p50 = 0x07,resolution_1280x720p30 = 0x08,resolution_1280x720p25 = 0x09,
	resolution_1280x720p24 = 0x0A,resolution_1920x1080i60 = 0x0B,resolution_1920x1080i59 = 0x0C,
	resolution_1920x1080i50 = 0x0D,resolution_1920x1080p60 = 0x0E,resolution_1920x1080p59 = 0x0F,
	resolution_1920x1080p50 = 0x10,resolution_1920x1080p30 = 0x011,resolution_1920x1080p25 = 0x12,
	resolution_1920x1080p24 = 0x13,resolution_640x480p60 = 0x14,resolution_640x480p75 = 0x15,
	resolution_800x600p60 = 0x16,resolution_800x600p75 = 0x17,resolution_1024x768p60 = 0x18,
	resolution_1024x768p75 = 0x19,resolution_1280x1024p60 = 0x1A,resolution_1280x1024p75 = 0x1B,
	resolution_1360x768p60 = 0x1C,resolution_1366x768p60 = 0x1D,resolution_1400x1050p60 = 0x1E,
	resolution_1600x1200p60 = 0x1F,resolution_1440x900p60 = 0x20,resolution_1440x900p75 = 0x021,
	resolution_1680x1050p60 = 0x22,resolution_1680x1050pRB = 0x23,resolution_1920x1080pRB = 0x24,
	resolution_1920x1200pRB = 0x25,resolution_1280x800p60 = 0x26,
	VIDOUT_FORMAT_END = 0x27,
};

//色彩空间
enum
{
	COLOR_SPACE_RGB				=0x00,	
	COLOR_SPACE_YUV444			=0x01,
	COLOR_SPACE_YUV422			=0x02,
	COLOR_SPACE_BT656			=0x03,
};

//输入信号
enum
{
	INPORT_VIDEO_NOSIGNAL 				=0x00,
	INPORT_VIDEO_HDMI_SIGNAL			=0x01,
	INPORT_VIDEO_VGA_SIGNAL				=0x02,
	INPORT_VIDEO_TRICOMPONENT_SIGNAL 	        =0x03,
	INPORT_VIDEO_CVBS_SIGNAL			=0x04,

};

//输入音源
enum
{
	INPORT_AUDIO_HDMI				= 0x00,
	INPORT_AUDIO_IIS 				= 0x01,

};

//  视频输出控制端
#ifdef __FUNCTION_VIDEO_OUT_INFO__
typedef struct _SB_video_out_info_
{
	unsigned char	resolution;			//输出分辨率
	unsigned char	colorspace;			//输出色彩空间
	unsigned char	brightness;			//输出亮度
	unsigned char	contrast;			//输出对比度
	unsigned char	saturation;			//输出饱和度
	unsigned char	hue;				//输出色度
	unsigned char	VGA_offset[4];			//输出vga 信号偏移坐标
	unsigned char	analogsignal_gain_R;		//输出模拟信号gain-R
	unsigned char	analogsignal_gain_G;		//输出模拟信号gain-G
	unsigned char	analogsignal_gain_B;		//输出模拟信号gain-B
	unsigned char	analogsignal_offset_R;		//输出输出模拟信号offset_R
	unsigned char	analogsignal_offset_G;		//模拟信号offset_G
	unsigned char	analogsignal_offset_B;		//输出模拟信号offset_B

}SB_video_out_info_t,*pSB_video_out_info_t;
#endif

//  视频输入端
#ifdef __FUNCTION_VIDEO_IN_INFO__
typedef struct _SB_video_in_info_
{
	unsigned char	resolution;				//输入分辨率
	unsigned char	colorspace;				//输入色彩空间
	unsigned char	port_input;				//输入有效端口

}SB_video_in_info_t,*pSB_video_in_info_t;
#endif

//   基本信息
#ifdef __FUNCTION_BASIC_INFO__
typedef struct _SB_basic_info_t_
{
	unsigned char*	name;					//板卡名字
	unsigned char*	style;					//板卡类型
	unsigned char	version[4];				//板卡版本信息
	unsigned char	site[2];				//板卡位置信息
	unsigned char*	osd;				        //OSD字幕

}SB_basic_info_t,*pSB_basic_info_t;
#endif

//  控制信息 
#ifdef __FUNCTION_CONTORL_INFO__
typedef struct _SB_contorl_info_
{
	unsigned char	screen_OnOff;				//画面开关
	unsigned char	screen_pause;				//画面暂停
	unsigned char	screen_update;				//升级开关
	unsigned char	ir_OnOff;				//红外开关	
	unsigned char	usart_OnOff;				//串口开关
	unsigned char	event_OnOff;				//异动事件报告开关

}SB_contorl_info_t,*pSB_contorl_info_t;
#endif

//   输入音频控制信息 
#ifdef __FUNCTION_AUDIO_IN_INFO__
typedef struct _SB_audio_in_info_
{
	unsigned char	audio_source;				//输入音频源


}SB_audio_in_info_t,*pSB_audio_in_info_t;
#endif


//   板卡全部信息结构体
typedef struct _SB_info_
{	
	unsigned int 			sb_syc_config_flag;
	unsigned char 			sb_info_inport;
	unsigned char 			sb_info_outport;
        
#ifdef __FUNCTION_VIDEO_IN_INFO__	
	pSB_video_out_info_t 	video_out_info;
#endif
        
#ifdef __FUNCTION_VIDEO_OUT_INFO__	
	pSB_video_in_info_t 	video_in_info;
#endif
        
#ifdef __FUNCTION_AUDIO_IN_INFO__	
	pSB_audio_in_info_t 	audio_in_info;
#endif
        
#ifdef __FUNCTION_BASIC_INFO__	
	pSB_basic_info_t		basic_info;
#endif	
        
#ifdef __FUNCTION_CONTORL_INFO__	
	pSB_contorl_info_t  	contorl_info;
#endif	
	
}SB_info_t,*pSB_info_t;


extern pSB_info_t pCurrent_SB_info[SB_HARDWARE_INPORT+SB_HARDWARE_OUTPORT];


int SB_CONFIG_INIT(void);
pSB_info_t malloc_SB_info(void);
void  memset_SB_info(pSB_info_t pSB_info);



#pragma pack ()
#ifdef __cplusplus
}
#endif

#endif



