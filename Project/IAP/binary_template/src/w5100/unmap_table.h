/**********************************************************************************************************************************************
*--------------------------------------------------umap_table-----------------------------------------------*
*--------------------------------------------------verion 1.1-----------------------------------------------*
*----------------------------------------------2015年8月25日16:02:19----------------------------------------*
*-----------------------------------------------author:lxb--------------------------------------------------*
*-------------------------------------------------user manual-----------------------------------------------*
**********************************************************************************************************************************************/

//------------------------------------------------- __MULTIPLE_ACCESS__------------------------------------------//
/**********************************************************************************************************************************************
*pUNMAP_TABLE_HEAD create_unmap_table(int user_number_total, ...);
*创建一个unmap，并申请空间
*------------------------------------------参数结构------------------------------------------------------//

*------user_number_total------------不定参数1---------------不定参数2------------ -----不定参数3--------------- -不定参数4---------依此类推//
*------最大用户数--------用户1的最大块数-----用户1的块的buf的大小----用户2的最大块数----用户2的块的buf的大小--依此类推// 
*--int get_from_table(pUNMAP_TABLE_HEAD phead, unsigned char* buf, int bufsize);
*--从unmap 中获取数据并数据长度  
*------phead-----------------------------buf---------------------------bufsize-------------------------返回值--------------- -//
*---unmap的参数指针--------用户传入的buf指针-----用户传入的buf的大小-----实际返回的数据长度-----// 
*--int set_to_table(pUNMAP_TABLE_HEAD phead,int user_number,unsigned char* buf, int bufsize);
*--将bufsize 数据长度的buf 写入unmap 中   
*------phead------------------------user_number--------------buf---------------------------bufsize-------------------------返回值--------------- -//
*---umap的参数指针-----所使用的user编号---用户传入的buf指针-----用户传入的buf的大小-----实际写入的数据长度-----// 
*--void delete_unmap_table(pUNMAP_TABLE_HEAD pHead);
*--删除unmap，释放所占用的空间  
*------phead--------------------------------- -//
*---unmap的参数指针 
**********************************************************************************************************************************************/
//------------------------------------------------- __MULTIPLE_ACCESS__------------------------------------------//


//------------------------------------------------- __SINGLE_ACCESS__------------------------------------------//
/**********************************************************************************************************************************************
*                                 unmap定义
*           -------------------------------------------------------
*  1、      |  块1  |  块2  |  块3  |  ...    ...    ...  |  块n  |      先进先出
*           -------------------------------------------------------
*
*  2、     长度为n的数组，用来标记块中是否存放了数据（一一对应）
*           -------------------------------------------------------
*           |   1   |   0   |   0   |  ...    ...    ...  |   0   |
*           -------------------------------------------------------
*      
*  3、     当要存放数据块时，从上次存放的位置的下一个开始，先判断对应标记是否为0，是0则存放数据并标记为1，否则向后移一位，继续判断
*
*          当要从unmap取出数据块时，从上次取数据块的位置的下一个开始，先判断对应标记是否为1，是1则取出数据并标记为0，否则向后移一位，继续判断
*
*  4、     使用举例:
*          pUNMAP_TABLE_HEAD   udp1_unmap;
*          udp1_unmap = (pUNMAP_TABLE_HEAD)malloc(64);   //为结构体分配内存空间
*          udp1_unmap = create_unmap_table(20, 128);     //创建
*
*          set_to_table(udp1_unmap, sock_proc_buf, len);
*          len = get_from_table(udp_unmap, udp_buf, 64);
*
***********************************************************************************************************************************************/
//------------------------------------------------- __SINGLE_ACCESS__------------------------------------------//



#ifndef __UNMAP_TABLE_H_
#define __UNMAP_TABLE_H_
#ifdef __cplusplus
extern "C" {
#endif

#include <stdarg.h>
#include <stdio.h>
#include "string.h"
#include <stdarg.h>
#include <stdlib.h>

#define MAX_UNMAP_CREAT_SIZE 1280
#define MAX_UNMAP_CREAT_TABLE 10


#pragma pack (1)

//#define __MULTIPLE_ACCESS__
#define __SINGLE_ACCESS__ 

#ifdef __MULTIPLE_ACCESS__ 
typedef struct _UNMAP_TABLE_HEAD
{	
    unsigned int    user_max_number;          //最大用户数量
    unsigned int    alluser_get_num;	      //读块值的位号在使用中始终加1，
    unsigned int    alluser_max_index;        //所有用户需求最大块数   
    unsigned char*  alluser_index_flag;       //所有用户需求块的使用标志
    unsigned int*   alluser_used_size;        //所有用户需求块每个buf空间已经使用的长度;
    unsigned char** alluser_index_list;       //用户需求buf指针数组
    unsigned int*   singleuser_set_num;       //不同的用户在写块值的组位号，在使用中始终加1
    unsigned int*   singleuser_max_index;     //不同用户需求最大块数
    unsigned int*   singleuser_index_size;    //不同的用户需求块对应的buf长度


}UNMAP_TABLE_HEAD,*pUNMAP_TABLE_HEAD;


#endif

#ifdef __SINGLE_ACCESS__ 
typedef struct _UNMAP_TABLE_HEAD
{	
    unsigned int get_num;
    unsigned int set_num;
    
    unsigned int max_index;
    unsigned int index_size;
    unsigned char* index_flag;
    unsigned int*  used_size;    //每个空间使用的有效长度;
    unsigned char** index_list;

	
}UNMAP_TABLE_HEAD,*pUNMAP_TABLE_HEAD;
#endif


/*
typedef struct _LIST_HEAD
{
	int total_size;
	int unused_size;
	int count;

}LIST_HEAD,*pLIST_HEAD;

*/
extern pUNMAP_TABLE_HEAD global_unmap_t;

void BSP_UNMAP_Init();

#ifdef __MULTIPLE_ACCESS__

pUNMAP_TABLE_HEAD create_unmap_table(int user_number_total, ...);
int get_from_table(pUNMAP_TABLE_HEAD phead, unsigned char* buf, int bufsize);
int set_to_table(pUNMAP_TABLE_HEAD phead,int user_number,unsigned char* buf, int bufsize);
void delete_unmap_table(pUNMAP_TABLE_HEAD pHead);

#endif

#ifdef __SINGLE_ACCESS__ 
pUNMAP_TABLE_HEAD create_unmap_table(int max_index,int size_of_index);

int get_from_table(pUNMAP_TABLE_HEAD pHead,unsigned char* buf,int bufsize);

int set_to_table(pUNMAP_TABLE_HEAD pHead,unsigned char* buf,int bufsize);

void delete_unmap_table(pUNMAP_TABLE_HEAD pHead);
#endif

#pragma pack ()
#ifdef __cplusplus
}
#endif

#endif



