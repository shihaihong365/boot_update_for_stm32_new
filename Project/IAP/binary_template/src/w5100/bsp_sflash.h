
#include  <stm32f10x_conf.h>

#ifndef __BSP_SFLASH_H__
#define __BSP_SFLASH_H__

#define	BSP_SFLASH_SECTOR_SIZE	4096
#define	BSP_SFLASH_PAGE_SIZE	256
#define	BSP_SFLASH_PAGECOUNT	512

#define	SFLASH_SST25VF016B_ID	0x00BF2541

void BSP_SFLASH_Init(void);
void BSP_SFLASH_Erase(unsigned int pos, unsigned int size);
unsigned int BSP_SFLASH_Read(unsigned int pos, void* buffer, unsigned int size);
unsigned int BSP_SFLASH_Write(unsigned int pos, const void* buffer, unsigned int size);
unsigned int BSP_SFLASH_ReadID(void);


#endif
