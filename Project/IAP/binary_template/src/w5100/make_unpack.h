#ifndef __MAKE_UNPACK__
#define __MAKE_UNPACK__

#include "make_pack.h"
#include "single_board_info.h"   // 板卡信息结构体文件
#include "app_file_table.h"

#include "file_pack.h"
#include "make_pack.h"
#include "iap.h"

extern fie_desc_info_t * g_total_fie_desc_info_t;
extern char g_get_file_step;
extern char * file_table_buf;
extern unsigned int file_table_len;
extern char g_total_fie_desc_info_buf[512];

unsigned char make_unpack_5555_Process(unsigned char *buf, unsigned int len);

#endif