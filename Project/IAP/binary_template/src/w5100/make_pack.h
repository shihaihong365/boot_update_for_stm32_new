#ifndef __MAKE_PACK_H__
#define __MAKE_PACK_H__

#ifdef __cplusplus
extern "C" {
#endif

#pragma pack(1)//设定为4字节对齐
#define _FUNCTION_PACKET_5555_

#ifdef _FUNCTION_PACKET_5555_
//注意协议的单位都为BYTE 所以将两个BYTE合成一个short的时候需要进行移位操作以交换顺序//
typedef struct _pack_5555_head_
{
	unsigned short syn;
	unsigned short pack_len;
	unsigned short pack_count;
	unsigned char  method;
	unsigned short cmd;
	unsigned short cmd_parameter_len;
	
}pack_5555_head_t,*pPack_5555_head_t;

//METHOD 
#define METHOD_GET            0x01
#define	METHOD_SET	      0x02
#define	METHOD_RETURN_GET     0x11
#define	METHOD_RETURN_SET     0x12
#define	METHOD_EVENT          0x05
//CMD
#define	CMD_GLOBAL_ALL 		 				0x00FE
#define	CMD_GLOBAL_INPORT 	 				0x0001
#define	CMD_GLOBAL_OUTPORT 	 				0x0002
#define	CMD_GLOBAL_VERSION 	 				0x0004
#define	CMD_GLOBAL_SN 		 				0x0005
#define	CMD_GLOBAL_STARTCOUTER				0x0006
#define	CMD_GLOBAL_BUZZER 		 			0x0007
#define	CMD_GLOBAL_CONFIG_COM 		 		0x0008
#define	CMD_GLOBAL_CONFIG_NET 		 		0x0009
#define	CMD_GLOBAL_TEMPERATURE 		 		0x000A
#define	CMD_GLOBAL_LANGUAGE 		 		0x000B

#define	CMD_SCREEN_SWITCH_ALL 		 		0x01FE
#define	CMD_SCREEN_SWITCH_INPORT 		 	0x0101
#define	CMD_SCREEN_SWITCH_OUTPORT		 	0x0102
#define	CMD_SCREEN_SWITCH_STATUSTABLE		0x0103
#define	CMD_SCREEN_SWITCH_STATUSMAP 		0x0104

#define	CMD_BOARDCARD_ONLINE_ALL 		 	0x02FE
#define	CMD_BOARDCARD_ONLINE_INPORT 		0x0201
#define	CMD_BOARDCARD_ONLINE_OUTPORT		0x0202
#define	CMD_BOARDCARD_ONLINE_STATUSTABLE	0x0203
#define	CMD_BOARDCARD_ONLINE_STATUSSINGLE	0x0204

#define	CMD_BOARDCARD_CONFIG_ALL  		 	0x03FE
#define	CMD_BOARDCARD_CONFIG_INPORT 		0x0301
#define	CMD_BOARDCARD_CONFIG_OUTPORT		0x0302
#define	CMD_BOARDCARD_CONFIG_ID 			0x0303

#define	CMD_SENCE_SWITCH_ALL 		 		0x04FE
#define	CMD_SENCE_SWITCH_NUMBER		 		0x0401
#define	CMD_SENCE_SWITCH_ID_GAIN		 	0x0402
#define	CMD_SENCE_SWITCH_ID_LOAD		 	0x0403
#define	CMD_SENCE_SWITCH_ID_SAVE		 	0x0404

//注意协议的单位都为BYTE 所以将两个BYTE合成一个short的时候需要进行移位操作以交换顺序//
#define EXCHANGE16BIT(X) ((((unsigned short)(X) & 0xff00) >> 8) |(((unsigned short)(X) & 0x00ff) << 8))

#define EXCHANGE32BIT(x) ((unsigned int)(				\
	(((unsigned int)(x) & (unsigned int)0x000000ffUL) << 24) |		\
	(((unsigned int)(x) & (unsigned int)0x0000ff00UL) <<  8) |		\
	(((unsigned int)(x) & (unsigned int)0x00ff0000UL) >>  8) |		\
	(((unsigned int)(x) & (unsigned int)0xff000000UL) >> 24)))


int make_pack_5555(unsigned char method, unsigned short cmd, unsigned short cmd_parameter_len, unsigned char* cmd_parameter_data_in, unsigned char * buf_out);
#endif

#pragma pack()//恢复对齐状态;

#ifdef __cplusplus
}
#endif

#endif
