
//#include <includes.h>
#include "bsp_touch.h"
//#include "v_iic.h"
//OS_SEM BSP_TOUCH_ISR_Sem;

void BSP_TOUCH_Init(void)
{
	#if 0
	GPIO_InitTypeDef	GPIO_InitStructure;
	SPI_InitTypeDef		SPI_InitStructure;
	NVIC_InitTypeDef	NVIC_InitStructure;
	EXTI_InitTypeDef	EXTI_InitStructure;

	RCC_APB2PeriphClockCmd(
		TP_NCS_GPIO_CLK	|
		TP_INT_GPIO_CLK	|
		TP_SPI_GPIO_CLK	|
		RCC_APB2Periph_AFIO,
		ENABLE
	);
	
	//
    GPIO_InitStructure.GPIO_Pin = TP_NCS_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_Out_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(TP_NCS_GPIO_PORT, &GPIO_InitStructure);
	//
    GPIO_InitStructure.GPIO_Pin = TP_SPI_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(TP_SPI_GPIO_PORT, &GPIO_InitStructure);
	//
    GPIO_InitStructure.GPIO_Pin = TP_INT_PIN;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(TP_INT_GPIO_PORT, &GPIO_InitStructure);
	
	//NVIC
	NVIC_InitStructure.NVIC_IRQChannel = TP_INT_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	GPIO_EXTILineConfig(TP_INT_PORT_SOURCE, TP_INT_PIN_SOURCE);
	
	EXTI_InitStructure.EXTI_Line = TP_INT_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
    EXTI_ClearITPendingBit(TP_INT_LINE);
	//
	//ysw
	BSP_IntVectSet(TP_INT_ID, BSP_TP_ISR);
	//BSP_IntEn(TP_INT_ID);
	#endif
	//

	
	/*
	if(TP_SPI == SPI1)
		RCC_APB2PeriphClockCmd(TP_SPI_CLK, ENABLE);
	else
		RCC_APB1PeriphClockCmd(TP_SPI_CLK, ENABLE);
	
	#ifdef	TP_SPI_REMAP
				GPIO_PinRemapConfig(TP_SPI_REMAP, ENABLE);
	#endif
	
	SPI_InitStructure.SPI_Direction = SPI_Direction_2Lines_FullDuplex;
	SPI_InitStructure.SPI_Mode = SPI_Mode_Master;
	SPI_InitStructure.SPI_DataSize = SPI_DataSize_8b;
	SPI_InitStructure.SPI_CPOL = SPI_CPOL_Low;
	SPI_InitStructure.SPI_CPHA = SPI_CPHA_1Edge;
	SPI_InitStructure.SPI_NSS = SPI_NSS_Soft;
	SPI_InitStructure.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64;
	SPI_InitStructure.SPI_FirstBit = SPI_FirstBit_MSB;
	SPI_InitStructure.SPI_CRCPolynomial = 7;
	SPI_Init(TP_SPI, &SPI_InitStructure);
	SPI_Cmd(TP_SPI, ENABLE);
	SPI_CalculateCRC(TP_SPI, DISABLE);
	
	BSP_TOUCH_cs_set();
	
	BSP_TOUCH_cs_clr();
	BSP_TOUCH_IO(1<<7);
	BSP_TOUCH_cs_set();
	*/

/*
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);


	GPIO_EXTILineConfig(GPIO_PortSourceGPIOB, GPIO_PinSource8);


	EXTI_InitStructure.EXTI_Line = EXTI_Line8;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);
	//EXTI_ClearITPendingBit(EXTI_Line8);
	//
	//ysw
	//BSP_IntVectSet(BSP_INT_ID_EXTI15_10, BSP_TP_ISR);
	BSP_IntVectSet(BSP_INT_ID_EXTI9_5,inttest);
	BSP_IntEn(BSP_INT_ID_EXTI9_5);
  


*/

//	GT811_Init();
//	BSP_OS_SemCreate(&BSP_TOUCH_ISR_Sem, 1, "touch SEM");
	
}

void BSP_TOUCH_IntEnable(unsigned char state)
{
APP_TRACE("BSP_TOUCH_IntEnable\r\n");
#if 0
	EXTI_InitTypeDef	EXTI_InitStructure;
	
	EXTI_InitStructure.EXTI_Line = TP_INT_LINE;
    EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
    EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling;
	EXTI_InitStructure.EXTI_LineCmd = (FunctionalState)state;
	EXTI_Init(&EXTI_InitStructure);
    EXTI_ClearITPendingBit(TP_INT_LINE);
#endif
}

unsigned char BSP_TOUCH_IO(unsigned char data)
{
APP_TRACE("BSP_TOUCH_IO\r\n");
#if 0
	while (SPI_I2S_GetFlagStatus(TP_SPI, SPI_I2S_FLAG_TXE) == RESET);
	SPI_I2S_SendData(TP_SPI, data);
	while (SPI_I2S_GetFlagStatus(TP_SPI, SPI_I2S_FLAG_RXNE) == RESET);
	data = SPI_I2S_ReceiveData(TP_SPI);
	return data;
#endif
}

void BSP_TOUCH_Read(unsigned short *x, unsigned short *y)
{
APP_TRACE("BSP_TOUCH_Read\r\n");

#if 0
	BSP_TOUCH_cs_clr();
	BSP_TOUCH_IO(TOUCH_MSR_X);
	*x = BSP_TOUCH_IO(0x00) << 4;
	*x |= ((BSP_TOUCH_IO(TOUCH_MSR_Y) >> 4) & 0x0F);
	*y = BSP_TOUCH_IO(0x00) << 4;
	*y |= ((BSP_TOUCH_IO(0x00) >> 4) & 0x0F);
	BSP_TOUCH_IO(1<<7);
	BSP_TOUCH_cs_set();
#endif
}

void BSP_TP_ISR(void)
{
APP_TRACE("BSP_TP_ISR\r\n");
#if 0
	//EXTI_ClearITPendingBit(TP_INT_LINE);
	BSP_TOUCH_INT_DIS();
	BSP_OS_SemPost(&BSP_TOUCH_ISR_Sem);
#endif
}
