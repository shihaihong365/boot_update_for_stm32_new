/**
  ******************************************************************************
  * @file    SPI/M25P64_FLASH/spi_flash.h
  * @author  MCD Application Team
  * @version V3.1.0
  * @date    06/19/2009
  * @brief   Header for spi_flash.c file.
  ******************************************************************************
  * @copy
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2009 STMicroelectronics</center></h2>
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SPI_FLASH_H
#define __SPI_FLASH_H

/* Includes ------------------------------------------------------------------*/
//#include "stm32f10x.h"
//#include "board.h"
#include  <stm32f10x_conf.h>

/* Private typedef -----------------------------------------------------------*/
#if 1	/* SST25VF016B */
#define SPI_FLASH_PAGESIZE    (4*1024)

//#define	BSP_SFLASH_SECTOR_SIZE	(4*1024)
//#define	BSP_SFLASH_PAGE_SIZE	256

#else
#define SPI_FLASH_PAGESIZE    256
#endif

/* Private define ------------------------------------------------------------*/

/* SPI Flash supported commands */
#if 1	/* SST25VF016B */
#define CMD_AAI       0xAD  /* AAI �������ָ�� */
#define CMD_DISWR	    0x04	/* ��ֹд, �˳�AAI״̬ */

#define CMD_EWRSR	    0x50	/* ����д״̬�Ĵ��� */
#define CMD_WRSR      0x01  /* Write Status Register instruction */
#define CMD_WREN      0x06  /* Write enable instruction */
#define CMD_READ      0x03  /* Read from Memory instruction */
#define CMD_RDSR      0x05  /* Read Status Register instruction  */
#define CMD_RDID      0x9F  /* Read identification */
#define CMD_SE        0x20  /* Sector Erase instruction */
#define CMD_BE        0xC7  /* Bulk Erase instruction */

#define WIP_FLAG      0x01  /* Write In Progress (WIP) flag */

#define DUMMY_BYTE    0xA5

#else	/* 	M25P64 */
#define CMD_WRITE     0x02  /* Write to Memory instruction */
#define CMD_WRSR      0x01  /* Write Status Register instruction */
#define CMD_WREN      0x06  /* Write enable instruction */
#define CMD_READ      0x03  /* Read from Memory instruction */
#define CMD_RDSR      0x05  /* Read Status Register instruction  */
#define CMD_RDID      0x9F  /* Read identification */
#define CMD_SE        0xD8  /* Sector Erase instruction */
#define CMD_BE        0xC7  /* Bulk Erase instruction */

#define WIP_FLAG      0x01  /* Write In Progress (WIP) flag */

#define DUMMY_BYTE    0xA5

#endif


/* Exported types ------------------------------------------------------------*/
/* Exported constants --------------------------------------------------------*/
/* Uncomment the line corresponding to the STMicroelectronics evaluation board
   used to run the example */
#if !defined (USE_STM3210B_EVAL) &&  !defined (USE_STM3210E_EVAL)
 //#define USE_STM3210B_EVAL
 //#define USE_STM3210E_EVAL
#endif

//onboard spi flash define
#define BSP_SPIFLASH_SPI            SPI1
#define BSP_SPIFLASH_SPI_CLK        RCC_APB2Periph_SPI1
#define BSP_SPIFLASH_SPI_GPIO_CLK   RCC_APB2Periph_GPIOA
#define BSP_SPIFLASH_SPI_GPIO       GPIOA
#define BSP_SPIFLASH_SPI_PIN_SCK    GPIO_Pin_5
#define BSP_SPIFLASH_SPI_PIN_MISO   GPIO_Pin_6
#define BSP_SPIFLASH_SPI_PIN_MOSI   GPIO_Pin_7

#define BSP_SPIFLASH_CS_CLK         RCC_APB2Periph_GPIOA
#define BSP_SPIFLASH_CS_GPIO        GPIOA
#define BSP_SPIFLASH_CS_PIN         GPIO_Pin_4

/* Defines for the SPI and GPIO pins used to drive the SPI Flash */
#define SPI_FLASH                 BSP_SPIFLASH_SPI
#define SPI_FLASH_CLK             BSP_SPIFLASH_SPI_CLK
#define SPI_FLASH_GPIO            BSP_SPIFLASH_SPI_GPIO
#define SPI_FLASH_GPIO_CLK        BSP_SPIFLASH_SPI_GPIO_CLK
#define SPI_FLASH_PIN_SCK         BSP_SPIFLASH_SPI_PIN_SCK
#define SPI_FLASH_PIN_MISO        BSP_SPIFLASH_SPI_PIN_MISO
#define SPI_FLASH_PIN_MOSI        BSP_SPIFLASH_SPI_PIN_MOSI

#define SPI_FLASH_CS_GPIO_CLK     BSP_SPIFLASH_CS_CLK
#define SPI_FLASH_CS_GPIO         BSP_SPIFLASH_CS_GPIO
#define SPI_FLASH_CS              BSP_SPIFLASH_CS_PIN

//#if defined (USE_STM3210B_EVAL)
// #define SPI_FLASH_CS             GPIO_Pin_4
// #define SPI_FLASH_CS_GPIO        GPIOA
// #define SPI_FLASH_CS_GPIO_CLK    RCC_APB2Periph_GPIOA
//#elif defined (USE_STM3210E_EVAL)
// #define SPI_FLASH_CS             GPIO_Pin_2
// #define SPI_FLASH_CS_GPIO        GPIOB
// #define SPI_FLASH_CS_GPIO_CLK    RCC_APB2Periph_GPIOB
//#endif

/* Exported macro ------------------------------------------------------------*/
/* Select SPI FLASH: Chip Select pin low  */
#define SPI_FLASH_CS_LOW()       GPIO_ResetBits(SPI_FLASH_CS_GPIO, SPI_FLASH_CS)
/* Deselect SPI FLASH: Chip Select pin high */
#define SPI_FLASH_CS_HIGH()      GPIO_SetBits(SPI_FLASH_CS_GPIO, SPI_FLASH_CS)

/* Exported functions --------------------------------------------------------*/
/*----- High layer function -----*/
void SPI_FLASH_Init(void);
void SPI_FLASH_EraseSector(unsigned long SectorAddr);
void SPI_FLASH_EraseBulk(void);
void SPI_FLASH_WritePage(unsigned char* pBuffer, unsigned long WriteAddr, unsigned short NumByteToWrite);
void SPI_FLASH_WriteBuffer(unsigned char* pBuffer, unsigned long WriteAddr, unsigned short NumByteToWrite);
void SPI_FLASH_ReadBuffer(unsigned char* pBuffer, unsigned long ReadAddr, unsigned short NumByteToRead);
unsigned long SPI_FLASH_ReadID(void);
void SPI_FLASH_StartReadSequence(unsigned long ReadAddr);
void SPI_FLASH_WriteStatus(unsigned char data);

/*----- Low layer function -----*/
unsigned char SPI_FLASH_ReadByte(void);
unsigned char SPI_FLASH_SendByte(unsigned char byte);
unsigned short SPI_FLASH_SendHalfWord(unsigned short HalfWord);
void SPI_FLASH_WriteEnable(void);
void SPI_FLASH_WaitForWriteEnd(void);

#endif /* __SPI_FLASH_H */

/******************* (C) COPYRIGHT 2009 STMicroelectronics *****END OF FILE****/
